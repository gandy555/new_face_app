#ifndef __WORKQUEUE_H__
#define __WORKQUEUE_H__

/******************************************************************************
 * Variable Type Definition
 ******************************************************************************/
#define INFINITE        0xFFFFFFFF
#define UNDELAYED       0
#define DELAYED         1

#define MAX_NAME_SIZE   20

typedef void (*work_handler)(size_t _param);
typedef void (*fail_handler)(void);

typedef struct workqueue_info {
    uint32_t p_time;
    uint32_t c_time;
    uint32_t wait_time;
    uint32_t retry_cnt;
    size_t param;
    int delete_pending_f;
    fail_handler fail_func;
    work_handler work_func;
} workqueue_info_t;

typedef struct workqueue_node{
    struct workqueue_info curr;
    struct workqueue_node* prev;
    struct workqueue_node* next;
} workqueue_node_t;

typedef struct {
    struct workqueue_node *head;
    struct workqueue_node *tail;
    uint32_t lock;
    int cnt;
    char name[MAX_NAME_SIZE];
} workqueue_list_t;

/******************************************************************************
 * Function Export (low level functions)
 ******************************************************************************/
#ifdef  __cplusplus
extern "C" {                        /* Use "C" linkage when in C++ mode */
#endif 
extern workqueue_node_t* workqueue_find_node(
        workqueue_list_t *_list_h, work_handler _wq_func);
extern int workqueue_delete_node(
        workqueue_list_t *_list_h, workqueue_node_t* _node);

/******************************************************************************
 * Function Export
 ******************************************************************************/
extern uint8_t workqueue_list_dump(workqueue_list_t *_list_h);
extern uint8_t workqueue_delete_all(workqueue_list_t *_list_h);
extern void workqueue_refresh_time(workqueue_node_t *_node);
extern void workqueue_reset_time(workqueue_node_t *_node);
extern int workqueue_reset_time_by_workfunc(workqueue_list_t *_list_h, work_handler work_func);
extern int workqueue_head_proc(workqueue_list_t *_list_h);
extern int workqueue_all_proc(workqueue_list_t *_list_h);
extern workqueue_node_t *workqueue_register(
    workqueue_list_t *_list_h, workqueue_info_t *_wq_info);
extern workqueue_node_t *workqueue_register_delayed(workqueue_list_t *_list_h, uint32_t _period,
    uint32_t _cnt, size_t _param, fail_handler fail_func, work_handler work_func);
extern workqueue_node_t *workqueue_register_undelayed(workqueue_list_t *_list_h, uint32_t _period,
    uint32_t _cnt, size_t _param, fail_handler fail_func, work_handler work_func);
extern uint8_t workqueue_unregister(
    workqueue_list_t *_list_h, work_handler work_func);
extern int workqueue_unregister_by_node(workqueue_list_t *_list_h, workqueue_node_t *_node);
extern uint8_t workqueue_check(
    workqueue_list_t *_list_h, work_handler work_func);
extern workqueue_node_t *workqueue_register_default(workqueue_list_t *_list_h, 
    size_t _param, work_handler _wq_func);
extern workqueue_list_t *workqueue_create(char *_name);
extern uint8_t workqueue_destroy(workqueue_list_t **_handle);
extern int register_workqueue_scheduler(uint32_t _period, work_handler _handler);
extern int unregister_workqueue_scheduler(work_handler _handler);
extern int unregister_workqueue_scheduler_wait_done(work_handler _handler);
extern int init_workqueue_scheduler(void);
extern int deinit_workqueue_scheduler(void);
#ifdef  __cplusplus
}                                   /* End of "C" linkage for C++       */
#endif

#endif // __WORKQUEUE_H__


#ifndef __UTILS_H__
#define __UTILS_H__

#include "dirent.h"
#include <sys/stat.h>
#include <time.h>

#ifndef bool
typedef unsigned char bool;
#define true 1
#define false 0
#endif

bool isFile(const char *path);
int copyFile(const char *src, const char *dst);
uint64_t getFreeMemSize();
#endif // __UTILS_H__
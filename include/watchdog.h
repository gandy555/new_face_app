#ifndef __WATCHDOG_H__
#define __WATCHDOG_H__

/******************************************************************************
 * Variable Type Definition
 ******************************************************************************/

/******************************************************************************
 * Function Export
 ******************************************************************************/
extern int wdt_init(void);
extern int wdt_set_period(uint32_t _period);
extern int wdt_start(void);
extern int wdt_stop(void);
extern int wdt_refresh(void);
extern void wdt_reboot(void);
extern int wdt_get_boot_status(void);
#endif //__WATCHDOG_H__


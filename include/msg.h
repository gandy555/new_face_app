#if !defined(__MSG_H__)
#define __MSG_H__

#include <stdint.h>

/******************************************************************************
 * Variable Type Definition
 ******************************************************************************/
enum {
    HMSG_NONE = -1,
    HMSG_TEST_GPIO,
    HMSG_TEST_I2C,
    HMSG_TEST_UART,
    HMSG_TEST_SPI,
    HMSG_TEST_TCPIP,
    HMSG_TEST_FTP,
    HMSG_TEST_UDP,
    HMSG_TEST_PLAYBACK,
    HMSG_TEST_MIPI,
    HMSG_TEST_HDMI,
    HMSG_TEST_PCI,
    HMSG_TEST_FWUP,
    HMSG_WDT_START,
    HMSG_WDT_STOP,
    HMSG_WDT_KICK,
    HMSG_STORAGE_INSERTED,
    HMSG_STORAGE_REMOVED,
    HMSG_SHOW_MEM_INFO,
    HMSG_PWR_REBOOT,
    HMSG_PWR_SHUTDOWN,
    HMSG_MAX
};

typedef struct {
    long type;
    int msg_id;
    int32_t param[2];
} msg_queue_t;

/******************************************************************************
 * Export Functions 
 ******************************************************************************/
#ifdef  __cplusplus
extern "C" {                        /* Use "C" linkage when in C++ mode */
#endif 
extern int msg_send(int32_t _msg, int32_t _param1, int32_t _param2);
extern int msg_rcv(msg_queue_t *_msg);
extern void msg_init(void);
#ifdef  __cplusplus
}                                   /* End of "C" linkage for C++       */
#endif

#endif	// __MSG_H__



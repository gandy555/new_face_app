#ifndef __SPI_DEV_H__
#define __SPI_DEV_H__

/******************************************************************************
 * Variable Type Definition
 ******************************************************************************/
/******************************************************************************
 * Function Export
 ******************************************************************************/
extern int spi_init(char *filename);
extern int spi_read(int add1, int add2, int nbytes, char *buffer, int file);
extern int spi_write(int add1, int add2, int nbytes, char *value, int file);
#endif //__SPI_DEV_H__


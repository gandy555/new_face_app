#ifndef __PORT_H__
#define __PORT_H__

#include <stdint.h>
#define PLATFORM_LINUX

/*
 *   OS Abstract Layer
 */

#define DUMMY_FUNCTION      do {} while (0)

#define __FILENAME__ ({	\
		const char *ptr = strrchr(__FILE__, '/'); \
		if (ptr) ptr++;	\
		else ptr = __FILE__; ptr; })

typedef unsigned int THREAD_TYPE;
typedef unsigned int EVENT_TYPE;
#if defined(PLATFORM_LINUX)
typedef timer_t TIMER_TYPE;
#else
typedef unsigned int TIMER_TYPE;
#endif
typedef int MSG_KEY;
typedef unsigned int port_time_t;
typedef void * MUTEX_P;
typedef void (*thread_handler)(void *);
typedef void (*tmr_handler)(void *);

#ifndef SEEK_SET
/* whence values for lseek(2) */
#define	SEEK_SET	0	/* set file offset to offset */
#define	SEEK_CUR	1	/* set file offset to current plus offset */
#define	SEEK_END	2	/* set file offset to EOF plus offset */
#endif

enum {
    CO_RED = 0,
    CO_GREEN,
    CO_BROWN,
    CO_BLUE,
    CO_PURPLE,
    CO_CYAN,
    CO_DEFAULT,
    CO_MAX
};

/*
 * TX_NO_WAIT, Immediate return
 */
#define PORT_EVT_NO_WAIT   0

/*
 * TX_WAIT_FOREVER, waits indefinitely
 */
#define PORT_EVT_WAIT_FOREVER  ((uint32_t) 0xFFFFFFFF)

/*
 * TX_OR, At least one requested event flag must be set in the specified event flags group
 */
#define PORT_EVT_OR    0

/*
 * TX_OR_CLEAR, At least one requested event flag must be set in the specified event flags group;
 * event flags that satisfy the request are cleared
 */
#define PORT_EVT_OR_CLR    1

/*
 * TX_AND, All requested event flags must be set in the specified event flags group
 */
#define PORT_EVT_AND   2

/*
 * TX_AND_CLEAR, All requested event flags must be set in the specified event flags group;
 * event flags that satisfy the request are cleared
 */
#define PORT_EVT_AND_CLR   3

#define PORT_RES_OK    0
#define PORT_RES_FAIL    -1

typedef struct {
    uint8_t state;
    uint8_t *stack;
    uint32_t stack_size;
    uint32_t priority;
    char name[128];
    thread_handler handler;
    THREAD_TYPE tsk_id;
} port_thread_info_t;

typedef struct {
	TIMER_TYPE tmr_id;
	tmr_handler handler;
	uint32_t args;
	uint32_t ticks;
} port_timer_info_t;

typedef struct {
    int tm_sec;   /* Seconds */
    int tm_min;   /* Minutes */
    int tm_hour;  /* Hour (0--23) */
    int tm_mday;  /* Day of month (1--31) */
    int tm_mon;   /* Month (0--11) */
    int tm_year;  /* Year (calendar year minus 1900) */
    int tm_wday;  /* Weekday (0--6; Sunday = 0) */
    int tm_yday;  /* Day of year (0--365) */
    int tm_isdst; /* 0 if daylight savings time is not in effect) */
} port_tm_t;

typedef struct {
    long type;
    int msg_id;
    size_t param[2];
} port_mq_t;

#define port_assert(n)   ((n) == 0) ? \
    port_printf_co(CO_RED, "<%s > Assert at %d", __func__, __LINE__) : ((void) (0))


#if defined(PLATFORM_LINUX)
/***************************************************************************/
/*                  PLATFORM_LINUX                          */
/***************************************************************************/
#define __ARMCC_PACK__
#define __ATTRIB_PACK__ __attribute__ ((packed))
#ifndef MCHAR
#define MCHAR               char
#endif
#elif defined(PLATFORM_UITRON)  /* PLATFORM_UITRON */
/***************************************************************************/
/*                  PLATFORM_UITRON                         */
/***************************************************************************/
#ifndef __ARMCC_PACK__
#define __ARMCC_PACK__ __packed
#define __ATTRIB_PACK__
#endif

#ifndef MCHAR
#define MCHAR   unsigned short
#endif

#else   /* PLATFORM_THREADX */
/***************************************************************************/
/*                  PLATFORM_THREADX                            */
/***************************************************************************/
#ifndef __ARMCC_PACK__
#define __ARMCC_PACK__
#define __ATTRIB_PACK__ __attribute__ ((packed))
#endif

#ifndef MCHAR
#define MCHAR   char
#endif
#endif

//---------------------------------------------------------------------------------
// Function Name  : ANALYSE_FUNC()
// Description    : analyse the spending time
//---------------------------------------------------------------------------------
#define ANALYSE_FUNC(_func)			\
{											\
    uint32_t s_time, c_time;							\
    								                    \
    s_time = port_mono_time();				\
    _func;									\
    c_time = port_mono_time();	        		\
    port_printf_co(CO_RED, "<%s> line: %d, spending time: %d ms", __func__, __LINE__, c_time - s_time);	\
}

//---------------------------------------------------------------------------------
// Function Name  : ANALYSE_LOOP_START()
// Description    : analyse the loop time
//---------------------------------------------------------------------------------
#define ANALYSE_LOOP_START(time)			\
{											\
    time = port_mono_time();				\
}

//---------------------------------------------------------------------------------
// Function Name  : ANALYSE_LOOP_END()
// Description    : analyse the loop time
//---------------------------------------------------------------------------------
#define ANALYSE_LOOP_END(time)			\
{										\
    uint64_t c_time;							\
                                                                       \
    c_time = port_mono_time();	        	\
    port_printf_co(CO_RED, "<%s> loop spending time: %d ms", __func__, c_time - time);	\
}

//---------------------------------------------------------------------------------
// Function Name  : ANALYSE_CALLED_PERIOD()
// Description    : analyse the call period
//---------------------------------------------------------------------------------
#define ANALYSE_CALLED_PERIOD(time)			\
{										\
    uint64_t c_time;							\
                                                                       \
    c_time = port_mono_time();	        	\
    port_printf_co(CO_RED, "<%s> call period: %d ms", __func__, c_time - time);	\
    time = c_time;                   \
}

/******************************************************************************
 * Function Export
 ******************************************************************************/
#ifdef  __cplusplus
extern "C" {                        /* Use "C" linkage when in C++ mode */
#endif 
extern int port_fw_path(char *_prefix, char *_path);
extern uint64_t port_mono_time(void);
extern uint64_t port_mono_elapsed_time(uint64_t p_time);
extern void port_curr_time(port_tm_t *_loc_time);
extern void port_localtime(const port_time_t *t, port_tm_t *_result);
extern void port_gmtime(const port_time_t *t, port_tm_t *_result);
extern int port_is_need_time_sync(void);
extern void port_set_gmtoffset(int sec);
extern int port_get_gmtoffset(void);
extern int port_set_time_dst(int dst);
extern int port_get_time_dst(void);
extern void port_time_str(port_tm_t *_time, char *_str);
/* local time의 tm 구조체를 입력 받아 seconds 값으로 변환단. offset 반영 */
extern port_time_t port_mktime(port_tm_t *tp);
/* tm 구조체를 입력 받아 seconds 값으로 변환단. offset은 무시 */
extern port_time_t port_tm_to_time(port_tm_t *tp);
/*
  set_rtc_time 함수의 입력값은 Local time이어야 한다.
*/
extern int port_set_rtc_time(port_tm_t *tp);
/*
  get_rtc_time 함수의 결과값은 Local time이다.
*/
extern int port_get_rtc_time(port_tm_t *tp);
extern port_time_t port_time(port_tm_t *tp);
extern int port_exist_file(const char * _path);
extern int port_rcv_msg(MSG_KEY msg_key, port_mq_t *_msg);
extern int port_create_msg(MSG_KEY *msg_key);
extern int port_destroy_msg(MSG_KEY *msg_key);
extern int port_create_mutex(MUTEX_P *mutex);
extern int port_destroy_mutex(MUTEX_P *mutex);
extern int port_mutex_lock(MUTEX_P mutex);
extern int port_mutex_unlock(MUTEX_P mutex);
extern int port_create_thread(port_thread_info_t *_tsk_info, void *param);
extern int port_destroy_thread(port_thread_info_t *_tsk_info);
extern int port_send_msg(MSG_KEY msg_key, uint32_t msg, uint32_t param1, uint32_t param2);
extern void port_printf(const char *fmt, ...);
extern void port_printf_co(int color, const char *fmt, ...);
extern void port_printf_nnt(const char *fmt, ...);
extern void *port_malloc(int size);
extern void port_free(void *mem);
extern void port_mem_pool_info(void);
extern void port_msleep(uint32_t ms);
extern FILE *port_fopen(const char *name, const char *mode);
extern int port_fwrite(const void *ptr, uint32_t size, uint32_t nobj, FILE *stream);
extern int port_fread(void *ptr, uint32_t size, uint32_t nobj, FILE *stream);
extern char *port_fgets(char *ptr, uint32_t bsize, FILE *stream);
extern int port_fseek(FILE *stream, long long offset, int origin);
extern uint32_t port_ftell(FILE *stream);
extern int port_fsync(FILE *stream);
extern int port_access(const char *_path, int mode);
extern int port_fclose(FILE *stream);
extern uint32_t port_copy(const char * src_file, const char * dst_file);
extern void port_ascii2uni(const char *ascstr, uint16_t *unistr,  int len);
extern int port_mkdir(const char *name);
extern int port_remove(const char *name);
extern void port_path_strcat(char *_dst, char *_append);
extern int port_copy_romfs(const char *_src_path, const char *_dst_path);
extern int port_fread_romfs(void *ptr, uint32_t size, uint32_t nobj, const char *_path);
extern int port_create_timer(port_timer_info_t *_tmr_info);
extern int port_delete_timer(port_timer_info_t *_tmr_info);
extern int port_stop_timer(port_timer_info_t *_tmr_info);
extern int port_start_timer(port_timer_info_t *_tmr_info);
extern int port_create_event(uint32_t *eid);
extern int port_delete_event(uint32_t eid);
extern int port_wait_event(uint32_t eid, uint32_t type, uint32_t option, uint32_t *flags, uint32_t timeout);
extern int port_set_event(uint32_t eid, uint32_t type);
extern int port_clr_event(uint32_t eid, uint32_t type);
extern int port_check_event(uint32_t eid, uint32_t type);
extern int port_file_size(const char *path);
extern int port_rand_in_range(int min, int max);
extern int port_copy_file(const char *src, const char *dst);
#ifdef  __cplusplus
}                                   /* End of "C" linkage for C++       */
#endif

#endif // __PORT_H__


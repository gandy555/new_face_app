#ifndef __CONTROLLER_GPIO_H__
#define __CONTROLLER_GPIO_H__

/******************************************************************************
 * Variable Type Definition
 ******************************************************************************/
#define GPIO1   0
#define GPIO2   32
#define GPIO3   64
#define GPIO4   96
#define GPIO5   128
#define GPIO6   160 
#define GPIO7   192
#define GPIO8   224
#define GPIO9   256
#define GPIO10   288

enum {
    GPIO_IN = 0,
    GPIO_OUT
};

/******************************************************************************
 * Function Export
 ******************************************************************************/
extern void gpio_config(u32 _port, u32 _pin, u32 _dir);
extern void gpio_set(u32 _port, u32 _pin);
extern u8 gpio_get(u32 _port, u32 _pin);
extern void gpio_clr(u32 _port, u32 _pin);
#endif //__CONTROLLER_GPIO_H__

/******************************************************************************
 * Filename:
 *   workqueue.c
 *
 * Description:
 *   workqueue
 *
 * Author:
 *   gandy
 *
 * Version : V0.1_19-11-12
 * ---------------------------------------------------------------------------
 * Abbreviation
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "port.h"
#include "workqueue.h"

/******************************************************************************
 *
 * Variable Declaration
 *
 ******************************************************************************/
//#define WORKQUEUE_DBG_ENABLE
#define WORKQUEUE_ERR_DBG_ENABLE

#if defined(WORKQUEUE_DBG_ENABLE)
#define WORKQUEUE_DBG(fmt, args ...)        port_printf_co(CO_BLUE, "[WORKQUEUE] " fmt, ## args)  
#else
#define WORKQUEUE_DBG(fmt, args ...)    
#endif

#if defined(WORKQUEUE_ERR_DBG_ENABLE)
#define WORKQUEUE_ERR_DBG(fmt, args ...)        port_printf_co(CO_RED, "[WORKQUEUE] " fmt, ## args)  
#else
#define WORKQUEUE_ERR_DBG(fmt, args ...)    
#endif

#define MAX_WORKQUEUE_LIST      100

#define HANDLE_ASSERT(h) {                          \
    if (h == NULL)  {                                   \
        port_printf_co(CO_RED, "<%s> Invalid Handle", __func__);   \
        return 0;                                   \
    }                                               \
}

#define WQ_THREAD_PRIORITY      180
#define WQ_THREAD_STACK_SIZE        (0x4000)
#define WQ_THREAD_NAME          "Workqueue Scheduler"

struct wq_scheduler_info {
    port_thread_info_t thread;
    MSG_KEY msg_key;
    int init_done;
};

enum {
    WQ_MSG_REGISTER_HANDLER = 0,
    WQ_MSG_UNREGISTER_HANDLER
};

static struct wq_scheduler_info s_wq_scheduler_info;
static workqueue_list_t *s_workqueue_scheduler = NULL;
#define WQ_SCHEDULER_H  s_workqueue_scheduler

/******************************************************************************
 *
 * Functions Declaration
 *
 ******************************************************************************/
int workqueue_delete_node(workqueue_list_t *_list_h, 
                    workqueue_node_t* _node);
workqueue_node_t* workqueue_find_node(workqueue_list_t *_list_h, 
                    work_handler _wq_func);
//------------------------------------------------------------------------------
// Function Name  : workqueue_append_node()
// Description    :
//------------------------------------------------------------------------------
static void workqueue_append_node(workqueue_list_t *_list_h, 
            workqueue_node_t* _node)
{
    _node->next = NULL;
    _node->prev = NULL;
    if (_list_h->head == NULL) {
        _list_h->head = _node;
        _list_h->tail = _node;
    } else {
        _node->prev = _list_h->tail;
        _list_h->tail->next = _node;
        _list_h->tail = _node;
    }
    _list_h->cnt += 1;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_make_node()
// Description    :
//------------------------------------------------------------------------------
static workqueue_node_t* workqueue_make_node(
                                workqueue_info_t* _info)
{
    workqueue_node_t* new_node = NULL;
    
    new_node = (workqueue_node_t*)port_malloc(sizeof(workqueue_node_t));
    if (new_node != NULL) {
        memset(new_node, 0, sizeof(workqueue_node_t));
        memcpy(&new_node->curr, _info, sizeof(workqueue_info_t));
    }
    return new_node;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_work()
// Description    :
//------------------------------------------------------------------------------
static uint8_t workqueue_work(workqueue_list_t *_list_h, workqueue_node_t* _node)
{
    _list_h->lock = 1;
    _node->curr.work_func(_node->curr.param);
    _node->curr.c_time = 0;
    if (_node->curr.retry_cnt != INFINITE) {
        _node->curr.retry_cnt--;
    }
    _list_h->lock = 0;
    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_expired()
// Description    :
//------------------------------------------------------------------------------
static uint8_t workqueue_expired(workqueue_list_t *_list_h, workqueue_node_t* _node)
{
    if (_node->curr.retry_cnt == 0) {
        if (_node->curr.fail_func != NULL)
            _node->curr.fail_func();
        return 1;
    }

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_list_dump()
// Description    :
//------------------------------------------------------------------------------
uint8_t workqueue_list_dump(workqueue_list_t *_list_h)
{
    workqueue_node_t* node;
    int i;

    HANDLE_ASSERT(_list_h);
    
    node = _list_h->head;
    WORKQUEUE_DBG("=================================");
    WORKQUEUE_DBG("= %s =", _list_h->name);
    WORKQUEUE_DBG("=================================");
    for (i = 0; i < _list_h->cnt; i++) {            
        WORKQUEUE_DBG("--------------------------------------");
        WORKQUEUE_DBG("wait_time: %d", node->curr.wait_time);
        WORKQUEUE_DBG("retry_cnt: %d", node->curr.retry_cnt);
        WORKQUEUE_DBG("work_func: %x", node->curr.work_func);           
        WORKQUEUE_DBG("--------------------------------------");            
        node = node->next;
    }
    WORKQUEUE_DBG("=================================");

    return 1;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_check_node()
// Description    :
//------------------------------------------------------------------------------
static int workqueue_check_node(workqueue_list_t *_list_h, workqueue_node_t *_node)
{
    workqueue_node_t *node;
    int i;

    HANDLE_ASSERT(_list_h);
    
    node = _list_h->head;
    for (i = 0; i < _list_h->cnt; i++) {
        if (node == _node) {
            return 0;
        }
        node = node->next;
    }

    return -1;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_find_node()
// Description    :
//------------------------------------------------------------------------------
workqueue_node_t* workqueue_find_node(workqueue_list_t *_list_h, 
                    work_handler _wq_func)
{
    workqueue_node_t* node;
    int i;

    HANDLE_ASSERT(_list_h);
    
    node = _list_h->head;
    for (i = 0; i < _list_h->cnt; i++) {
        if (node->curr.work_func == _wq_func) {
            return node;
        }
        node = node->next;
    }

    return (workqueue_node_t *)NULL;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_delete_node()
// Description    :
//------------------------------------------------------------------------------
int workqueue_delete_node(workqueue_list_t *_list_h, 
        workqueue_node_t* _node)
{
    if (_node == NULL) {
        WORKQUEUE_ERR_DBG("<%s> failed(%s: %d)", __func__, 
            _list_h->name, _list_h->cnt);
        return -1;
    }

    if (!_list_h->cnt) {
        WORKQUEUE_ERR_DBG("<%s> failed(%s: %d)", __func__, 
            _list_h->name, _list_h->cnt);
        return -1;
    }

    if (_list_h->lock) {
        WORKQUEUE_ERR_DBG("<%s> couldn't delete in work func (deleting will after work func)", __func__);
        _node->curr.delete_pending_f = 1;
        return -1;
    }
    
    if (_list_h->head == _node) {
        if (_node->next != NULL) {
            _list_h->head = _node->next;
            _list_h->head->prev = NULL;
        } else {
            _list_h->head = NULL;
            _list_h->tail = NULL;
        }
        _list_h->cnt -= 1;
        port_free(_node);          
    } else {        
        if (_list_h->tail == _node)
            _list_h->tail = _node->prev;

        if (_node->prev != NULL)
            _node->prev->next = _node->next;
        if (_node->next != NULL)
            _node->next->prev = _node->prev;
        
        _list_h->cnt -= 1;
        port_free(_node);
    }

    WORKQUEUE_DBG("<%s> %s(%d)", __func__, 
        _list_h->name, _list_h->cnt);
    
    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_register()
// Description    :
//------------------------------------------------------------------------------
workqueue_node_t *workqueue_register(workqueue_list_t *_list_h, 
    workqueue_info_t *_wq_info)
{
    workqueue_node_t *new_node = NULL;

    HANDLE_ASSERT(_list_h);
    
    WORKQUEUE_DBG("<%s> %s(%d)", __func__, 
        _list_h->name, _list_h->cnt);
        
    if (_list_h->cnt >= MAX_WORKQUEUE_LIST) {
        WORKQUEUE_ERR_DBG("list is full");
        return NULL;
    }
    
    new_node = workqueue_make_node(_wq_info);
    if (new_node == NULL) {
        WORKQUEUE_ERR_DBG("make failed");
        return NULL;
    }
    
    workqueue_append_node(_list_h, new_node);

    return new_node;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_register_default()
// Description    :
//------------------------------------------------------------------------------
workqueue_node_t *workqueue_register_default(workqueue_list_t *_list_h, 
    size_t _param, work_handler _wq_func)
{
    workqueue_info_t workqueue;
    
    HANDLE_ASSERT(_list_h);
    
    memset(&workqueue, 0, sizeof(workqueue_info_t));
    workqueue.p_time = 0;
    workqueue.c_time = 0;
    workqueue.wait_time = 3000;
    workqueue.retry_cnt = 1;
    workqueue.param = _param;
    workqueue.fail_func = NULL;
    workqueue.work_func = _wq_func;
    
    return workqueue_register(_list_h, &workqueue);
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_register_delayed()
// Description    :
//------------------------------------------------------------------------------
workqueue_node_t *workqueue_register_delayed(workqueue_list_t *_list_h, uint32_t _period,
    uint32_t _cnt, size_t _param, fail_handler fail_func, work_handler work_func)
{
    workqueue_info_t workqueue;
    
    HANDLE_ASSERT(_list_h);
    
    memset(&workqueue, 0, sizeof(workqueue_info_t));
    workqueue.p_time = 0;
    workqueue.c_time = 0;
    workqueue.wait_time = _period;
    workqueue.retry_cnt = _cnt;
    workqueue.param = _param;
    workqueue.fail_func = fail_func;
    workqueue.work_func = work_func;
    
    return workqueue_register(_list_h, &workqueue);
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_register_delayed()
// Description    :
//------------------------------------------------------------------------------
workqueue_node_t *workqueue_register_undelayed(workqueue_list_t *_list_h, uint32_t _period,
    uint32_t _cnt, size_t _param, fail_handler fail_func, work_handler work_func)
{
    workqueue_info_t workqueue;
    
    HANDLE_ASSERT(_list_h);
    
    memset(&workqueue, 0, sizeof(workqueue_info_t));
    workqueue.p_time = 0;
    workqueue.c_time = _period;
    workqueue.wait_time = _period;
    workqueue.retry_cnt = _cnt;
    workqueue.param = _param;
    workqueue.fail_func = fail_func;
    workqueue.work_func = work_func;
    
    return workqueue_register(_list_h, &workqueue);
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_unregister()
// Description    : 
//------------------------------------------------------------------------------
uint8_t workqueue_unregister(workqueue_list_t *_list_h, work_handler work_func)
{
    workqueue_node_t *node;

    HANDLE_ASSERT(_list_h);

    node = workqueue_find_node(_list_h, work_func);
    if (node != NULL) {
        workqueue_delete_node(_list_h, node);
        return 1;
    }

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_unregister()
// Description    : 
//------------------------------------------------------------------------------
int workqueue_unregister_by_node(workqueue_list_t *_list_h, workqueue_node_t *_node)
{
    HANDLE_ASSERT(_list_h);

    if (workqueue_check_node(_list_h, _node) == 0) {
        workqueue_delete_node(_list_h, _node);
        return 0;
    }

    return -1;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_check()
// Description    :
//------------------------------------------------------------------------------
uint8_t workqueue_check(workqueue_list_t *_list_h, work_handler work_func)
{
    workqueue_node_t *node;

    HANDLE_ASSERT(_list_h);
    
    node = workqueue_find_node(_list_h, work_func);
    if (node != NULL)
        return 1;
    
    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_delete_all()
// Description    :
//------------------------------------------------------------------------------
uint8_t workqueue_delete_all(workqueue_list_t *_list_h)
{
    workqueue_node_t *node = NULL, *n_node = NULL;
    int i, total_cnt;

    HANDLE_ASSERT(_list_h);
    WORKQUEUE_DBG("<%s> %s(%d)", __func__, 
        _list_h->name, _list_h->cnt);   
    
    node = _list_h->head;
    total_cnt = _list_h->cnt;
    for (i = 0; i < total_cnt; i++) {
        n_node = node->next;
        workqueue_delete_node(_list_h, node);
        node = n_node;
    }

    return 1;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_refresh_time()
// Description    : current time을 wait time에 맞춰 다음 tick에 work func가 호출되도록 한다.
//------------------------------------------------------------------------------
void workqueue_refresh_time(workqueue_node_t *_node)
{       
    _node->curr.c_time = _node->curr.wait_time;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_reset_time()
// Description    : current time을 초기화 하여 wait time 만큼 다시 기다리도록 한다.
//------------------------------------------------------------------------------
void workqueue_reset_time(workqueue_node_t *_node)
{       
    _node->curr.c_time = 0;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_reset_time_by_workfunc()
// Description    : current time을 초기화 하여 wait time 만큼 다시 기다리도록 한다.
//------------------------------------------------------------------------------
int workqueue_reset_time_by_workfunc(workqueue_list_t *_list_h, work_handler work_func)
{
    workqueue_node_t *node;
    
    node = workqueue_find_node(_list_h, work_func);
    if (node == NULL) {
        WORKQUEUE_ERR_DBG("workfunc was not found !!");
        return -1;
    }
    
    workqueue_reset_time(node);

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_head_proc()
// Description    : proccessing sequential work queue
//------------------------------------------------------------------------------
int workqueue_head_proc(workqueue_list_t *_list_h)
{
    workqueue_node_t* node = NULL, *n_node = NULL;
    uint64_t curr_time;
    int diff;

    HANDLE_ASSERT(_list_h);

    node = _list_h->head;
    if (node == NULL) {
        return 0;
    }

    curr_time = port_mono_time();    
    if (node->curr.p_time == 0)
        node->curr.p_time = curr_time;
    diff = curr_time - node->curr.p_time;
    node->curr.c_time += diff;
    node->curr.p_time = curr_time;
    if (node->curr.c_time > node->curr.wait_time) {
        workqueue_work(_list_h, node);
        if (workqueue_expired(_list_h, node))
            workqueue_delete_node(_list_h, node);
    }

    // delete pended work_func
    node = _list_h->head;
    if (node->curr.delete_pending_f) {  /* aleady deleted in lock state */
        WORKQUEUE_DBG("delete node by pending flag");
        workqueue_delete_node(_list_h, node);
    }

    return 1;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_all_proc()
// Description    : proccessing all of work queue
//------------------------------------------------------------------------------
int workqueue_all_proc(workqueue_list_t *_list_h)
{
    workqueue_node_t *node, *n_node = NULL;
    uint32_t curr_time, diff;

    HANDLE_ASSERT(_list_h);

    node = _list_h->head;
    if (node == NULL) {
        return 0;
    }

    curr_time = port_mono_time();    
    while (node != NULL) {
        if (node->curr.delete_pending_f) {  /* aleady deleted in lock state */
            WORKQUEUE_DBG("delete node by pending flag");
            n_node = node->next;
            workqueue_delete_node(_list_h, node);
        } else {
            if (node->curr.p_time == 0)
                node->curr.p_time = curr_time;

            diff = curr_time - node->curr.p_time;
            node->curr.c_time += diff;
            n_node = node->next;
            node->curr.p_time = curr_time;
            if (node->curr.c_time > node->curr.wait_time) {
                workqueue_work(_list_h, node);
                if (workqueue_expired(_list_h, node))
                    workqueue_delete_node(_list_h, node);    
            }
       }
       node = n_node;
    }

    // delete pended work_func
    node = _list_h->head;
    while (node != NULL) {
        n_node = node->next;
        if (node->curr.delete_pending_f) {  /* aleady deleted in lock state */
            WORKQUEUE_DBG("delete node by pending flag");
            workqueue_delete_node(_list_h, node);
        }
        node = n_node;
    }
    return 1;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_create()
// Description    : create the workqueue handle 
//------------------------------------------------------------------------------
workqueue_list_t *workqueue_create(char *_name)
{
    workqueue_list_t *new_h = NULL;

    if ((_name != NULL) && (strlen(_name) >= MAX_NAME_SIZE)) {
        WORKQUEUE_ERR_DBG("Invalid handle name");
        return NULL;
    }
    
    new_h = port_malloc(sizeof(workqueue_list_t));  
    if (new_h != NULL) {
        memset(new_h, 0, sizeof(workqueue_list_t));
        if (_name != NULL)
            strcpy(new_h->name, _name);
        else
            strcpy(new_h->name, "UNKNOWN");
    }

    WORKQUEUE_DBG("<%s> %s", __func__, new_h->name);
    return new_h;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_destroy()
// Description    : destroy the workqueue handle 
//------------------------------------------------------------------------------
uint8_t workqueue_destroy(workqueue_list_t **_handle)
{   
    uint8_t res = 0;
    workqueue_list_t *h = *_handle;
    
    if (h != NULL) {
        WORKQUEUE_DBG("<%s> %s", __func__, h->name);                
        workqueue_delete_all(h);
        port_free(h);
        *_handle = NULL;
        res = 1;
    }

    return res;
}

//------------------------------------------------------------------------------
// Function Name  : workqueue_thread()
// Description    :
//------------------------------------------------------------------------------
static void workqueue_thread(void *arg)
{
    port_thread_info_t *thread = (port_thread_info_t *)arg;
    workqueue_info_t *workqueue;
    port_mq_t msg;
    size_t *param;
    
    port_printf("<%s> start!!", __func__); 

    while (thread->state) {
        if (port_rcv_msg(s_wq_scheduler_info.msg_key, &msg) > 0) {
            param = (size_t *)(&msg.param[0]);     
            switch (msg.msg_id) {
            case WQ_MSG_REGISTER_HANDLER:
                workqueue = (workqueue_info_t *)param[0];
                workqueue_register(WQ_SCHEDULER_H, workqueue);
                port_free(workqueue);
                break;
            case WQ_MSG_UNREGISTER_HANDLER:
                workqueue_unregister(WQ_SCHEDULER_H, (work_handler)param[0]);
                break;
            default:
                break;
            }
        }

        workqueue_all_proc(WQ_SCHEDULER_H);
        
        port_msleep(10);
    }
}

//------------------------------------------------------------------------------
// Function Name  : register_workqueue_scheduler()
// Description    : 
//------------------------------------------------------------------------------
int register_workqueue_scheduler(uint32_t _period, work_handler _handler)
{
    workqueue_info_t *new_wq;

    new_wq = port_malloc(sizeof(workqueue_info_t));
    if (new_wq == NULL) {
        port_printf_co(CO_RED, "<%s> malloc failed", __func__);
        return -1;
    }

    memset(new_wq, 0, sizeof(workqueue_info_t));
    new_wq->c_time = _period;
    new_wq->wait_time = _period;
    new_wq->retry_cnt = INFINITE;
    new_wq->param = 0;
    new_wq->fail_func = NULL;
    new_wq->work_func = _handler;
    return port_send_msg(s_wq_scheduler_info.msg_key,
        WQ_MSG_REGISTER_HANDLER, (size_t)new_wq, 0);
}

//------------------------------------------------------------------------------
// Function Name  : unregister_workqueue_scheduler()
// Description    : 
//------------------------------------------------------------------------------
int unregister_workqueue_scheduler(work_handler _handler)
{
    return port_send_msg(s_wq_scheduler_info.msg_key,
        WQ_MSG_UNREGISTER_HANDLER, (size_t)_handler, 0);
}

//------------------------------------------------------------------------------
// Function Name  : unregister_workqueue_scheduler_wait_done() [blocked]
// Description    : Wait until handler unregister done
//------------------------------------------------------------------------------
int unregister_workqueue_scheduler_wait_done(work_handler _handler)
{
    #define UNREG_WORKQUEUE_WAIT_DONE_TIME_OUT  5000
    uint64_t start_time = port_mono_time();

    port_send_msg(s_wq_scheduler_info.msg_key,
        WQ_MSG_UNREGISTER_HANDLER, (size_t)_handler, 0);

    do{
        if (!workqueue_check(WQ_SCHEDULER_H, _handler))
            return 0;
        port_msleep(10);
    }while(port_mono_time() - start_time < UNREG_WORKQUEUE_WAIT_DONE_TIME_OUT);

    return -1;
}

//------------------------------------------------------------------------------
// Function Name  : init_workqueue_scheduler()
// Description    : 
//------------------------------------------------------------------------------
int init_workqueue_scheduler(void)
{
    struct wq_scheduler_info *scheduler = &s_wq_scheduler_info;
    
    port_printf_co(CO_BLUE, "<%s>", __func__);
    
    WQ_SCHEDULER_H = workqueue_create("Workqueue Scheduler");
    
    port_create_msg(&scheduler->msg_key);

    scheduler->thread.priority = WQ_THREAD_PRIORITY;
    scheduler->thread.stack_size = WQ_THREAD_STACK_SIZE;    
    scheduler->thread.handler = workqueue_thread;
    strcpy(scheduler->thread.name, WQ_THREAD_NAME);
    port_create_thread(&scheduler->thread, &scheduler->thread);    

    return 1;
}

//------------------------------------------------------------------------------
// Function Name  : deinit_workqueue_scheduler()
// Description    :
//------------------------------------------------------------------------------
int deinit_workqueue_scheduler(void)
{
    struct wq_scheduler_info *scheduler = &s_wq_scheduler_info;

    port_printf_co(CO_BLUE, "<%s>", __func__);

    if (WQ_SCHEDULER_H) {
        port_printf_co(CO_BLUE, "<%s> WQ_SCHEDULER_H", __func__);
        port_destroy_thread(&scheduler->thread);
        port_destroy_msg(&scheduler->msg_key);
        workqueue_destroy(&WQ_SCHEDULER_H);

        return 1;
    }

    return 0;
}


#ifndef __TCP_API_H__
#define __TCP_API_H__

extern int get_ip_address(char *_ip_addr);
extern int get_mac_address(char *_mac);
extern int tcp_connect(char *_addr, char *_port);
extern int tcp_send_data(int _sock, char *_data, int _size);
extern int tcp_rcv_data(int _sock, char* _buf, int _max_size, int _time_out);
extern int tcp_close(int _sock);


#endif // __TCP_API_H__
/******************************************************************************
 * Filename:
 *   msg.c
 *
 * Description:
 *   message module
 *
 * Author:
 *   gandy
 *
 * Version : V0.1_18-09-13
 * ---------------------------------------------------------------------------
 * Abbreviation
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/types.h>

#include "msg.h"


#define HCMGR_MSG_TYPE      5678


static key_t g_hcmgr_msg = 0;

//---------------------------------------------------------------------------------
// Function Name  : msg_send()
// Descryption    : 
//---------------------------------------------------------------------------------
int msg_send(int32_t _msg, int32_t _param1, int32_t _param2)
{
    msg_queue_t msg;

    memset(&msg, 0, sizeof(msg_queue_t));

    msg.type = HCMGR_MSG_TYPE;
    msg.msg_id = _msg;
    msg.param[0] = _param1;
    msg.param[1] = _param2;
    return msgsnd(g_hcmgr_msg, (void *)&msg, sizeof(msg_queue_t)-sizeof(long), IPC_NOWAIT);
}

//---------------------------------------------------------------------------------
// Function Name  : msg_rcv()
// Descryption    : 
//---------------------------------------------------------------------------------
int msg_rcv(msg_queue_t *_msg)
{
    return msgrcv(g_hcmgr_msg, (void *)_msg, sizeof(msg_queue_t)-sizeof(long),
        HCMGR_MSG_TYPE, IPC_NOWAIT);
}

//---------------------------------------------------------------------------------
// Function Name  : msg_init()
// Descryption    : 
//---------------------------------------------------------------------------------
void msg_init(void)
{
    msg_queue_t msg;
    
    if (g_hcmgr_msg == 0) {
        g_hcmgr_msg = msgget((key_t)5454, IPC_CREAT|0666);
        
        // discard garbage message..
        while (msg_rcv(&msg) > 0) {
        }
    }
}


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/sendfile.h>  // sendfile
#include <fcntl.h>         // open
#include <unistd.h>        // close
#include <sys/stat.h>      // fstat
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <stdint.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include "png.h"
#include "port.h"
#include "utils.h"
#include "main.h"

bool isFile(const char *path)
{
    struct stat path_stat;

    memset((void *)&path_stat, 0, sizeof(struct stat));
    stat(path, &path_stat);
    if (S_ISREG(path_stat.st_mode))
        return true;

    return false;
}

int copyFile(const char *src, const char *dst) {
    int source = open(src, O_RDONLY, 0);
    int dest = open(dst, O_WRONLY | O_CREAT, 0644);
    int res;

    // struct required, rationale: function stat() exists also
    struct stat stat_source;
    fstat(source, &stat_source);

    res = sendfile(dest, source, 0, stat_source.st_size);

    close(source);
    close(dest);

    return res;
}

uint64_t getFreeMemSize() {
   	FILE *fp;
	char *ptr, buf[128];
	uint32_t free_kb = 0, buffers_kb = 0, cached_kb = 0;

	fp = fopen("/proc/meminfo", "r");
	if(!fp) {
		return 0ull;
	}

	/*
	 # cat /proc/meminfo
		MemTotal:          55440 kB
		MemFree:            8360 kB
		Buffers:            3004 kB
		Cached:            10964 kB
		SwapCached:            0 kB
	*/

	while (fgets(buf, sizeof(buf), fp)) {
		if((ptr = strstr(buf, "MemFree:")) != NULL) {
			ptr += 8;
			while (*ptr != 0 && isspace(*ptr))
				ptr++;
			sscanf(ptr, "%u kB", &free_kb);
		} else if((ptr = strstr(buf, "Buffers:")) != NULL) {
			ptr += 8;
			while (*ptr != 0 && isspace(*ptr))
				ptr++;
			sscanf(ptr, "%u kB", &buffers_kb);
		} else if((ptr = strstr(buf, "Cached:")) != NULL) {
			ptr += 7;
			while (*ptr != 0 && isspace(*ptr))
				ptr++;
			sscanf(ptr, "%u kB", &cached_kb);
			break;
		}
	}

	fclose(fp);

	uint64_t total = (free_kb + buffers_kb + cached_kb) * 1024ll;

	//port_printf("%u %u %u %llu\n", free_kb, buffers_kb, cached_kb, total);

	return total;
}

#if defined(CONFIG_USE_LIB_PNG)
//load the png file using libpng.so
uint8_t *loadPng(const char *file_path)
{
	FILE *fp = NULL;
	unsigned char png_check[8] = { 0, };
	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL, end_info = NULL;
	png_bytep *row_pointers = NULL;
	png_uint_32 width, height;
	int32_t bit_depth, color_type, interlace_type;
	int32_t byte_depth;
	size_t row_bytes;
    uint8_t *pixels = NULL;
    
	// open file and check for PNG validity
	fp = fopen(file_path, "rb");
	if(!fp) {
		port_printf_co(CO_RED, "\"%s\" open error: %d(%s)\n", file_path, errno, strerror(errno));
		return NULL;
	}

	fread(png_check, 1, 8, fp);
	if(png_sig_cmp(png_check, 0, 8)) {
		port_printf_co(CO_RED, "\"%s\" seems not be type of png format.\n", file_path);
		goto failed;
	}

	// setup PNG structures for read
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr) {
		port_printf_co(CO_RED, "\"%s\" failed to initialize 'png_ptr' structure.\n", file_path);
		goto failed;
	}

	info_ptr = png_create_info_struct(png_ptr);
	if(!info_ptr) {
		port_printf_co(CO_RED, "\"%s\" failed to initialize 'info_ptr' structure.\n", file_path);
		goto failed;
	}

	end_info = png_create_info_struct(png_ptr);
	if(!end_info) {
		port_printf_co(CO_RED, "\"%s\" failed to initialize 'end_info' structure.\n", file_path);
		png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
		goto failed;
	}
	// error handling callback for png file reading
	if(setjmp(png_jmpbuf(png_ptr))) {
		port_printf_co(CO_RED, "\"%s\" encountered unknown fatal error in libpng.\n", file_path);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		goto failed;
	}

	png_init_io(png_ptr, fp);
	png_set_sig_bytes(png_ptr, 8);

	// get PNG header info up to data block
	png_read_info(png_ptr, info_ptr);

	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlace_type, (int32_t*)0, (int32_t*)0);
	port_printf("\"%s\": size %dx%d, %d bits, %d color, %d interlace\n", file_path, width, height, bit_depth, color_type, interlace_type);
	// "/data/button3.png": size 48x48, 8 bits, RGBAlpha(6) color, None(0) interlace

	// transforms to unify image data
	if(color_type == PNG_COLOR_TYPE_PALETTE) {
		png_set_palette_to_rgb(png_ptr);
		color_type = PNG_COLOR_TYPE_RGB;
		bit_depth = 8;
	}
	if(color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8) {
		png_set_expand_gray_1_2_4_to_8(png_ptr);
		bit_depth = 8;
	}
	if(png_get_valid(png_ptr,info_ptr, PNG_INFO_tRNS)) {
		png_set_tRNS_to_alpha(png_ptr);
		color_type |= PNG_COLOR_MASK_ALPHA;
	}
	if(color_type == PNG_COLOR_TYPE_GRAY || color_type==PNG_COLOR_TYPE_GRAY_ALPHA) {
		png_set_gray_to_rgb(png_ptr);
		color_type |= PNG_COLOR_MASK_COLOR;
	}
	if(color_type == PNG_COLOR_TYPE_RGB) {
		png_set_filler(png_ptr, 0xffffU, PNG_FILLER_AFTER);
	}

	if (color_type == PNG_COLOR_TYPE_RGB || color_type == PNG_COLOR_TYPE_RGB_ALPHA) {
		png_set_bgr(png_ptr);
	}

	png_read_update_info(png_ptr,info_ptr);

	if (bit_depth != 8 /*&& bit_depth != 16*/) {
		port_printf_co(CO_RED, "\"%s\" support only 8-bit depth. %d-bit depth not supported yet.\n", file_path, bit_depth);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		goto failed;
	}

	if (color_type != PNG_COLOR_TYPE_RGB && color_type != PNG_COLOR_TYPE_RGB_ALPHA) {
		port_printf_co(CO_RED, "\"%s\" support either RGB or RGBA. %d not supported yet.\n", file_path, color_type);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		goto failed;
	}

	//is_alpha_ = (color_type == PNG_COLOR_TYPE_RGBA);

	row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * height);
	if(row_pointers == NULL) {
		port_printf_co(CO_RED, "out of memory for 'row_pointers' %d bytes\n", sizeof(png_bytep) * height);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		goto failed;
	}

	byte_depth = bit_depth >> 3;
	row_bytes = png_get_rowbytes(png_ptr, info_ptr);
	for(int32_t y=0; y<height; y++) {
		row_pointers[y] = (png_bytep)malloc(row_bytes);
		if(row_pointers[y] == NULL) {
			port_printf_co(CO_RED, "out of memory for 'row_pointers[%d]' %zd bytes\n", y, row_bytes);
			for(int32_t i=0; i<y; i++) {
				free(row_pointers[i]);
			}
			free(row_pointers);
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
			goto failed;
		}
	}

	//PNG_TRANSFORM_SWAP_ALPHA: RGBA -> ARGB
	png_read_image(png_ptr, row_pointers);
	png_read_end(png_ptr, end_info);

	png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);

	// copy the image
	pixels = (uint8_t*)malloc(row_bytes*height);
	if (pixels) {
		uint8_t *row = pixels;
		for(int32_t y=0; y<height; y++, row += row_bytes) {
			memcpy(row, row_pointers[y], row_bytes);
		}
	} else {
		port_printf_co(CO_RED, "out of memory for 'pixels_' %d bytes.\n", row_bytes*height);
	}

	for(int32_t y=0; y<height; y++) {
		free(row_pointers[y]);
	}
	free(row_pointers);

	fclose(fp);
	fp = NULL;

	return pixels;

failed:

	if(fp != NULL) {
		fclose(fp);
	}

	return NULL;
}
#endif
/******************************************************************************
 * Filename:
 *   controller_watchdog.c
 *
 * Description:
 *   control the watchdog function
 *
 * Author:
 *   gandy
 *
 * Version : V0.1_15-08-25
 * ---------------------------------------------------------------------------
 * Abbreviation
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>
#include <signal.h>
#include <time.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/watchdog.h>

#include "type.h"
#include "port.h"
#include "watchdog.h"
/******************************************************************************
 *
 * Variable Declaration
 *
 ******************************************************************************/
#define WDT_DEV             "/dev/watchdog"
#define DEFAULT_WDT_TIME    15000

static int g_wdt_fd = -1;

#if 0
WDIOC_SETTIMEOUT
WDIOC_GETTIMEOUT
WDIOC_GETTIMELEFT
WDIOC_GETBOOTSTATUS
WDIOC_GETSTATUS
WDIOC_SETOPTIONS
#endif

/******************************************************************************
 *
 * Public Functions Declaration
 *
 ******************************************************************************/
int wdt_init(void);
int wdt_set_period(uint32_t _period);
int wdt_start(void);
int wdt_stop(void);
int wdt_refresh(void);
void wdt_reboot(void);
//------------------------------------------------------------------------------
// Function Name  : wdt_init()
// Description    :
//------------------------------------------------------------------------------
int wdt_init(void)
{
    g_wdt_fd = open(WDT_DEV, O_RDWR);
    if (g_wdt_fd < 0)   {
        port_printf("<%s> open failed(%s)", __func__, strerror(errno));
        return -1;
    }

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : wdt_set_period()
// Description    :
//------------------------------------------------------------------------------
int wdt_set_period(uint32_t _period)
{
    uint32_t timeout = _period;
    
    if (g_wdt_fd >= 0) {
        ioctl(g_wdt_fd, WDIOC_SETTIMEOUT, &timeout);
        return 0;
    }

    return -1;
}

//------------------------------------------------------------------------------
// Function Name  : wdt_start()
// Description    :
//------------------------------------------------------------------------------
int wdt_start(void)
{
    uint32_t timeout = DEFAULT_WDT_TIME;
    int flags = WDIOS_ENABLECARD;
    
    if (g_wdt_fd >= 0) {
        // set timeout
        ioctl(g_wdt_fd, WDIOC_SETTIMEOUT, &timeout); 
        // enable
        ioctl(g_wdt_fd, WDIOC_SETOPTIONS, &flags);
        return 0;
    }

    return -1;
}

//------------------------------------------------------------------------------
// Function Name  : wdt_stop()
// Description    :
//------------------------------------------------------------------------------
int wdt_stop(void)
{
    int flags = WDIOS_DISABLECARD;
    
    if (g_wdt_fd >= 0) {
        ioctl(g_wdt_fd, WDIOC_SETOPTIONS, &flags);
    }
    
    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : wdt_refresh()
// Description    :
//------------------------------------------------------------------------------
int wdt_refresh(void)
{
    if (g_wdt_fd >= 0) {
        ioctl(g_wdt_fd, WDIOC_KEEPALIVE, 0);
        return 0;
    }

    return -1;
}

//------------------------------------------------------------------------------
// Function Name  : wdt_reboot()
// Description    :
//------------------------------------------------------------------------------
void wdt_reboot(void)
{
    system("reboot");
}

//------------------------------------------------------------------------------
// Function Name  : wdt_get_boot_status()
// Description    :
//------------------------------------------------------------------------------
int wdt_get_boot_status(void)
{
    int res;
    
    if (g_wdt_fd >= 0)
        ioctl(g_wdt_fd, WDIOC_GETBOOTSTATUS, &res); 
    else
        return -1;
    
    port_printf_co(CO_BLUE, "<%s> 0x%X", __func__, res);
    if (res & 0x20) {
        port_printf_co(CO_RED, "WDT occured!!");
        return 1;
    }

    return 0;
}


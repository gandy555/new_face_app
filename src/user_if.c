/******************************************************************************
 * Filename:
 *   controller_serial.c
 *
 * Description:
 *   controller of uart driver 
 *
 * Author:
 *   gandy
 *
 * Version : V0.1_15-10-02
 * ---------------------------------------------------------------------------
 * Abbreviation
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#include "type.h"
#include "port.h"
#include "msg.h"

static pthread_t g_cli_thread;
static int g_cli_loop = 1;

//------------------------------------------------------------------------------
// Function Name  : cli_thread()
// Description    :
//------------------------------------------------------------------------------
static void show_menu(void)
{
    printf("\033[2J");		// clear console
    printf("\033[1;1H");	// move position of cursor
    printf("==================================\r\n");
    printf("= input character as below =\r\n");
    printf("= g: HMSG_TEST_GPIO (owner M4)\r\n");
    printf("= G: HMSG_TEST_GPIO (owner A53)\r\n");
    printf("= i: HMSG_TEST_I2C (owner M4)\r\n");
    printf("= I: HMSG_TEST_I2C (owner A53)\r\n");
    printf("= u: HMSG_TEST_UART\r\n");
    printf("= s: HMSG_TEST_SPI\r\n");
    printf("= t: HMSG_TEST_TCPIP\r\n");
    printf("= f: HMSG_TEST_FTP\r\n");
    printf("= U: HMSG_TEST_UDP\r\n");
    printf("= p: HMSG_TEST_PLAYBACK\r\n");
    printf("= m: HMSG_TEST_MIPI\r\n");
    printf("= h: HMSG_TEST_HDMI\r\n");
    printf("= P: HMSG_TEST_PCI\r\n");
    printf("= F: HMSG_TEST_FWUP\r\n");
}

//------------------------------------------------------------------------------
// Function Name  : cli_thread()
// Description    :
//------------------------------------------------------------------------------
static void *cli_thread(void *para)
{
    uint8_t in_cmd;
    
    port_printf("<%s> start !!", __func__);
    
    while (g_cli_loop) {
        in_cmd = getchar();
        switch (in_cmd) {
        case 'g':
            msg_send(HMSG_TEST_GPIO, 0, 0);
            break;
        case 'G':
            msg_send(HMSG_TEST_GPIO, 1, 0);
            break;
        case 'i':
            msg_send(HMSG_TEST_I2C, 0, 0);
            break;
        case 'I':
            msg_send(HMSG_TEST_I2C, 1, 0);
            break;
        case 'u':
            msg_send(HMSG_TEST_UART, 0, 0);
            break;
        case 's':
            msg_send(HMSG_TEST_SPI, 0, 0);
            break;
        case 't':
            msg_send(HMSG_TEST_TCPIP, 0, 0);
            break;
        case 'f':
            msg_send(HMSG_TEST_FTP, 0, 0);
            break;
        case 'U':
            msg_send(HMSG_TEST_UDP, 0, 0);
            break;
        case 'p':
            msg_send(HMSG_TEST_PLAYBACK, 0, 0);
            break;
        case 'm':
            msg_send(HMSG_TEST_MIPI, 0, 0);
            break;
        case 'h':
            msg_send(HMSG_TEST_HDMI, 0, 0);
            break;
        case 'P':
            msg_send(HMSG_TEST_PCI, 0, 0);
            break;
        case 'F':
            msg_send(HMSG_TEST_FWUP, 0, 0);
            break;
        default:
            show_menu();
            break;
        }
    }
}

//------------------------------------------------------------------------------
// Function Name  : user_if_init()
// Description    :
//------------------------------------------------------------------------------
int user_if_init(void)
{
    g_cli_loop = 1;
    int res = pthread_create(&g_cli_thread, NULL, cli_thread, NULL);
    if (res < 0) {
        port_printf("<%s> pthread_create failure: error=%d %s",
            __func__, errno, strerror(errno));
        return -1;
    }

    return 0;
}


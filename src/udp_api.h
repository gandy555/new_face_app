#ifndef __UDP_API_H__
#define __UDP_API_H__

extern int udp_server_start(char *_port);
extern int udp_server_rcv_data(int _sock, char* _buf, int _max_size, int _time_out);
extern int udp_client_start(char *_addr, char *_port);
extern int udp_send_data(int _sock, char* _buf, int _size);
extern int udp_close(int _sock);
#endif // __UDP_API_H__


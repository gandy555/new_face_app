#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <termios.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <linux/spi/spidev.h>
#include <pthread.h>
#include <sched.h>

#include "main.h"
#include "port.h"
#include "gpio.h"
#include "msg.h"
#include "utils.h"
#include "workqueue.h"
#include "watchdog.h"
#include "serial.h"
#include "user_if.h"
#include "tcp_api.h"
#include "udp_api.h"
#include "ftp_api.h"
#include "i2c-utils.h"
#include "spi_dev.h"

static int s_main_loop = 1;
static workqueue_list_t *s_main_workqueue;

#define MAIN_WQ_H s_main_workqueue
#define RPMSG_DEV   "/dev/ttyRPMSG30"

#define I2C_DEV "/dev/i2c-3"
#define I2C_SLAVE_ADDR 0x40
#define I2C_READ_REG 0x10

#define SPI_DEV "/dev/spidev1.1"

#define GPIO_PIN    15
/* 
    The AP and M4 communication protocols are fixed at 32 bytes.
    lexer : ,
    -------------------------------------------------
    peripheral_id(UART/I2C/...),opcode(w/r),ch(num/ch),data[32-prefix]
    ------------------------------------------------    
*/
#define TEST_CMD_GPIO   "GPIO,W,10,1\n"   // GPIO10_IO1 set to high
#define TEST_CMD_I2C   "I2C,W,1,10,1\n"   // i2c1 write 1 data to register 0x0a
#define TEST_CMD_UART   "UART,W,3,1234\n"   // write 1234 data to uart3  
#define TEST_CMD_SPI   "SPI,W,1,1234\n"   // write 1234 data to spi1

#define TCP_IP_ADDR     "10.10.81.147"
#define TCP_PORT_NUM     "23"

#define UDP_IP_ADDR     "10.10.81.147"
#define UDP_PORT_NUM     "4023"

#define FTP_IP_ADDR     "10.10.81.198"
#define FTP_PORT_NUM     "21"
#define FTP_USER_NAME   "gandy"
#define FTP_PASSWORD    "1234"

#define M4_MSG_SIZE     32

//---------------------------------------------------------------------------------
// Function Name  : unregister_user_signal()
// Descryption    :
//---------------------------------------------------------------------------------
void unregister_user_signal(void)
{
    signal (SIGABRT, SIG_IGN);
    signal (SIGSEGV, SIG_IGN);
    signal (SIGBUS, SIG_IGN);
    signal (SIGILL, SIG_IGN);
    signal (SIGFPE, SIG_IGN);
    signal (SIGPIPE, SIG_IGN);
    signal (SIGINT, SIG_IGN);
    signal (SIGTERM, SIG_IGN);
}

//---------------------------------------------------------------------------------
// Function Name  : user_signal_handler()
// Descryption    :
//---------------------------------------------------------------------------------
static void user_signal_handler(int signum, siginfo_t* si, void* unused)
{
    const char* name = NULL;

    switch (signum) {
    case SIGABRT: name = "SIGABRT";  break;
    case SIGSEGV: name = "SIGSEGV";  break;
    case SIGBUS:  name = "SIGBUS";   break;
    case SIGILL:  name = "SIGILL";   break;
    case SIGFPE:  name = "SIGFPE";   break;
    case SIGPIPE: name = "SIGPIPE";  break;
    case SIGINT:  name = "SIGINT";  break;
    case SIGTERM: name = "SIGTERM";  break;
    }

    if (name) {
        fprintf(stderr, "Caught signal %d (%s)\n", signum, name);
    } else {
        fprintf(stderr, "Unknown signal %d\n", signum);
    }

    s_main_loop = 0;

    sleep(1);

    exit(signum);
}

//---------------------------------------------------------------------------------
// Function Name  : register_user_signal()
// Descryption    :
//---------------------------------------------------------------------------------
static void register_user_signal(void)
{
    struct sigaction sa;
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = user_signal_handler;
    sigemptyset(&sa.sa_mask);

    sigaction( SIGABRT, &sa, NULL );
    sigaction( SIGSEGV, &sa, NULL );
    sigaction( SIGBUS,  &sa, NULL );
    sigaction( SIGILL,  &sa, NULL );
    sigaction( SIGFPE,  &sa, NULL );
    sigaction( SIGPIPE, &sa, NULL );
    sigaction( SIGINT,  &sa, NULL );
    sigaction( SIGTERM, &sa, NULL );
}

static void wdt_kicker(size_t p)
{
    msg_send(HMSG_WDT_KICK, 0, 0);
}

int main(int argc, char* argv[])
{
    msg_queue_t msg;
    int serial_fd;
    uint8_t buffer[M4_MSG_SIZE] = {0,};
    ftp_file_info_t file_info = {0,};
    int sock, res;
    int gpio_toggle = 1;
    int test_fd;

    // driver load...
    system("modprobe imx_rpmsg_tty");

    sleep(1);
    
    register_user_signal();

    gpio_config(GPIO4, GPIO_PIN, GPIO_OUT);

    // ipc msg initialize for communication with user (console)
    msg_init();

    // watchdog timer initialize
    wdt_init();

    // rpmsg initialize for communcation with m4-core
    serial_fd = serial_open(RPMSG_DEV, 115200);

    user_if_init();

    // user timer initialize for periodical wdt refresh.
    MAIN_WQ_H = workqueue_create((char *)"MAIN APP");
    workqueue_register_delayed(MAIN_WQ_H, 5000, INFINITE, 0,
        NULL, wdt_kicker);
    
    while (s_main_loop) {
        if (msg_rcv(&msg) > 0) {
            switch (msg.msg_id) {
            case HMSG_TEST_GPIO:
                port_printf("[HMSG_TEST_GPIO]");
                if (msg.param[0]) { 
                    // owner linux
                    gpio_toggle ^= 1;
                    if (gpio_toggle)
                        gpio_set(GPIO4, GPIO_PIN);
                    else
                        gpio_clr(GPIO4, GPIO_PIN);
                } else {    
                    // owner M4
                    memset(buffer, 0, M4_MSG_SIZE);
                    strcpy(buffer, TEST_CMD_GPIO);
                    serial_write(serial_fd, buffer, M4_MSG_SIZE);
                }
                break;
            case HMSG_TEST_I2C:
                port_printf("[HMSG_TEST_I2C]");
                if (msg.param[0]) { 
                    // owner linux
                    test_fd = open(I2C_DEV, O_RDWR);
                    if (test_fd >= 0) {
                        ioctl(test_fd, I2C_SLAVE, I2C_SLAVE_ADDR);
                        res = i2c_smbus_read_word_data(test_fd, I2C_READ_REG);
                        //i2c_smbus_write_word_data(test_fd, I2C_READ_REG, 0x6543);
                        
                        //i2c_smbus_read_byte(test_fd);
                        port_printf("I2C_READ(0x%x): 0x%x", I2C_READ_REG, res);
                        close(test_fd);
                    }
                } else {
                    // owner M4
                    memset(buffer, 0, M4_MSG_SIZE);
                    strcpy(buffer, TEST_CMD_I2C);
                    serial_write(serial_fd, buffer, M4_MSG_SIZE);
                }
                break;
            case HMSG_TEST_UART:
                port_printf("[HMSG_TEST_UART]");
                memset(buffer, 0, M4_MSG_SIZE);
                strcpy(buffer, TEST_CMD_UART);
                serial_write(serial_fd, buffer, M4_MSG_SIZE);
                break;
            case HMSG_TEST_SPI:
                port_printf("[HMSG_TEST_SPI]");
                if (msg.param[0]) {
                    // owner linux
                    test_fd = spi_init(SPI_DEV);
                    if (test_fd >= 0) {
                        buffer[0] = 0x41;
                        buffer[1] = 0xFF;
                        //this will write value 0x41FF to the address 0xE60E
                        spi_write(0xE6,0x0E, 2, buffer, test_fd); 
                        //reading the address 0xE60E
                        spi_read(0xE6, 0x0E, 4, buffer, test_fd);
                        close(test_fd);
                    }
                } else {
                    memset(buffer, 0, M4_MSG_SIZE);
                    strcpy(buffer, TEST_CMD_SPI);
                    serial_write(serial_fd, buffer, M4_MSG_SIZE);
                }
                break;
            case HMSG_TEST_TCPIP:
                port_printf("[HMSG_TEST_TCPIP] %s:%s", TCP_IP_ADDR, TCP_PORT_NUM);
                sock = tcp_connect(TCP_IP_ADDR, TCP_PORT_NUM);
                if (sock >= 0) {
                    memset(buffer, 0, sizeof(buffer));
                    strcpy(buffer, "hello new face TCP");
                    tcp_send_data(sock, buffer, strlen(buffer));
                    tcp_close(sock);
                }
                break;
            case HMSG_TEST_FTP:
                port_printf("[HMSG_TEST_FTP] %s:%s", FTP_IP_ADDR, FTP_PORT_NUM);
                sock = ftp_connect(FTP_IP_ADDR, FTP_PORT_NUM, FTP_USER_NAME, FTP_PASSWORD);
                if (sock >= 0) {
                    system("dmesg > ./ftp_test.txt");
                    memset(&file_info, 0, sizeof(ftp_file_info_t));
                    getcwd(file_info.path, 64);
                    strcpy(file_info.file_name, "ftp_test.txt");
                    ftp_file_upload(sock, &file_info);
                    ftp_close(sock);
                }
                break;
            case HMSG_TEST_UDP:
                port_printf("[HMSG_TEST_UDP]");
                #if 0
                sock = udp_server_start("23");
                if (sock >= 0) {
                    if ((res = udp_rcv_data(sock, buffer, 512, 3)) > 0) {
                        port_printf("RCVD: %d", res);
                    }
                    udp_close(sock);
                }
                #else
                sock = udp_client_start(UDP_IP_ADDR, UDP_PORT_NUM);
                if (sock >= 0) {
                    memset(buffer, 0, sizeof(buffer));
                    strcpy(buffer, "hello new face UDP");
                    udp_send_data(sock, buffer, strlen(buffer));
                    udp_close(sock);
                }
                #endif
                break;
            case HMSG_TEST_PLAYBACK:
                port_printf("[HMSG_TEST_PLAYBACK]");
                break;
            case HMSG_TEST_MIPI:
                port_printf("[HMSG_TEST_MIPI]");
                break;
            case HMSG_TEST_HDMI:
                port_printf("[HMSG_TEST_HDMI]");
                break;
            case HMSG_TEST_PCI:
                port_printf("[HMSG_TEST_PCI]");
                break;    
            case HMSG_WDT_KICK:
                port_printf("[HMSG_WDT_KICK]\r\n");
                wdt_refresh();
                break;
            case HMSG_WDT_START:
                port_printf("[HMSG_WDT_START]");
                wdt_start();
                break;    
            case HMSG_WDT_STOP:
                port_printf("[HMSG_WDT_STOP]");
                wdt_stop();
                break;
            case HMSG_STORAGE_INSERTED:
                port_printf("[HMSG_STORAGE_INSERTED]\r\n");
                break;
            case HMSG_STORAGE_REMOVED:
                port_printf("[HMSG_STORAGE_REMOVED]\r\n");
                break;
            case HMSG_SHOW_MEM_INFO:
                port_printf("[HMSG_SHOW_MEM_INFO]\r\n");
                port_printf("## Free memory : %lld Kbyte\n", getFreeMemSize()/1024);
                break;
            case HMSG_PWR_REBOOT:
                port_printf("[HMSG_PWR_REBOOT]\r\n");
                system("reboot");
                break;    
            case HMSG_PWR_SHUTDOWN:
                port_printf("[HMSG_PWR_SHUTDOWN]\r\n");
                system("power off");
                break;
            default:
                port_printf("[UNKOWN HMSG] %d\r\n", msg.msg_id);
                break;
            }
        }

        workqueue_all_proc(MAIN_WQ_H);
        usleep(10000);
    }

    unregister_user_signal();

    port_printf("@@@@@@@@@@@@ Terminate @@@@@@@@@@");
    port_printf("@@@@@@@@@@@@ Terminate @@@@@@@@@@");
    port_printf("@@@@@@@@@@@@ Terminate @@@@@@@@@@");

    return 0;
}

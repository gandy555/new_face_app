#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <time.h>

#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h> 
#include <syslog.h>
#include <netdb.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <sys/time.h>
#include <getopt.h>

#include "port.h"
#include "tcp_api.h"

//----------------------------------------------------------------------------
// Define
//----------------------------------------------------------------------------
#define MAX_CONNECTION_TIMEOUT 3
#define MAX_SEND_PKT_SIZE 1 * 1024 * 1024
#define MAX_DNS_CONV_INFO 10
#define MAX_DNS_CONV_FAIL_CNT 10

#define DNS_QUERY_CMD_STR "nslookup %s|grep \"Address 1\"|tail -1|awk -F \" \" '{print $3}' > %s"
#define DNS_QUERY_FILE "/tmp/dns_query"

//----------------------------------------------------------------------------
// Struct
//----------------------------------------------------------------------------
struct dns_conv_ip
{
    int enable;
    int fail_cnt;
    char dns_name[100];
    struct in_addr ip_addr;
};

struct dns_info
{
    int cnt;
    struct dns_conv_ip conv_ip[MAX_DNS_CONV_INFO];
};


//----------------------------------------------------------------------------
// variables
//----------------------------------------------------------------------------
struct dns_info g_dns_info;


//---------------------------------------------------------------------------------
// Function Name  : get_ip_address()
// Description    :
//---------------------------------------------------------------------------------
int get_ip_address(char *_ip_addr)
{
	int sock;
	struct ifreq ifr;
	struct sockaddr_in *sin;
	
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		return -1;
	}

	strcpy(ifr.ifr_name, "wlan0");
	if (ioctl(sock, SIOCGIFADDR, &ifr) < 0) {
		close(sock);
		return -1;
	}
	
	sin = (struct sockaddr_in*)&ifr.ifr_addr;
	strcpy(_ip_addr, inet_ntoa(sin->sin_addr));
	
	close(sock);
	return 1;
}

//---------------------------------------------------------------------------------
// Function Name  : convert_mac()
// Description    :
//---------------------------------------------------------------------------------
static void convert_mac(const char *_data, char *_cvrt_str, int _size)
{
    char buf[128] = {0,};
    char t_buf[8];
    char *stp = strtok((char *)_data , ":" );
    int temp=0;

    do
    {
        memset( t_buf, 0, sizeof(t_buf) );
        sscanf( stp, "%x", &temp );
        snprintf( t_buf, sizeof(t_buf)-1, "%02X", temp );
        strncat( buf, t_buf, sizeof(buf)-1 );
        strncat( buf, ":", sizeof(buf)-1 );
    } while( (stp = strtok( NULL , ":" )) != NULL );

    buf[strlen(buf) -1] = '\0';
    strncpy( _cvrt_str, buf, _size );
}

//---------------------------------------------------------------------------------
// Function Name  : get_mac_address()
// Description    :
//---------------------------------------------------------------------------------
int get_mac_address(char *_mac)
{
	int sock;
	struct ifreq ifr;
	char mac_adr[18] = {0,};
	
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		return 0;
	}

	strcpy(ifr.ifr_name, "wlan0");
	if (ioctl(sock, SIOCGIFHWADDR, &ifr)< 0) {
		close(sock);
		return 0;
	}
	
	//convert format ex) 00:00:00:00:00:00
	convert_mac(ether_ntoa((struct ether_addr *)(ifr.ifr_hwaddr.sa_data)), mac_adr, sizeof(mac_adr) - 1);
	  
	strcpy(_mac, mac_adr);
	
	close(sock);

	return 1;
}

//---------------------------------------------------------------------------------
// Function Name  : 
// Description    :
//---------------------------------------------------------------------------------
static int connect_wait(int _sock, struct sockaddr *_addr, int _size, int _sec)
{
    int flags; 
    int res, n; 
    fd_set  rset, wset; 
    struct timeval tval;
    int error = 0; 
    socklen_t esize; 
 
    flags = fcntl(_sock, F_GETFL, NULL);
    fcntl(_sock, F_SETFL, flags | O_NONBLOCK); // Non blocking 상태로 만든다.
 
    // 연결을 기다린다. 
    // Non blocking 상태이므로 바로 리턴한다. 
    if ((res = connect(_sock, _addr, _size)) < 0) { 
        if (errno != EINPROGRESS) 
            return -1;
    } 

    port_printf("<%s> %d", __func__, res);
    // 즉시 연결이 성공했을 경우 소켓을 원래 상태로 되돌리고 리턴한다.  
    if (res == 0) { 
        goto CONNECT_SUCCESS;
    } 
 
    FD_ZERO(&rset); 
    FD_SET(_sock, &rset);
    wset = rset;

    tval.tv_sec = _sec;
    tval.tv_usec = 0;
 
    if ((n = select(_sock+1, &rset, &wset, NULL, &tval)) == 0) { 
        // timeout 
        port_printf("Connect Timeout\n");
        errno = ETIMEDOUT;
        return -1;
    } 
 
    // 읽거나 쓴 데이터가 있는지 검사한다.  
    if (FD_ISSET(_sock, &rset) || FD_ISSET(_sock, &wset)) {
        //port_printf("Read data\n"); 
        esize = sizeof(error); 
        if (getsockopt(_sock, SOL_SOCKET, SO_ERROR, &error, &esize) < 0) {
            return -1;
        }
    } else { 
        port_printf("select error: _sock not set");
    } 

CONNECT_SUCCESS:
    port_printf("Connect Success\n");

    fcntl(_sock, F_SETFL, flags);  /* restore file status flags */
    if (error) {
        errno = error;
        return(-1);
    }
    return 1;

}

//---------------------------------------------------------------------------------
// Function Name  : upload_append_cmd()
// Descryption    :
//---------------------------------------------------------------------------------
static void set_dns_info(struct dns_conv_ip *_dns_info)
{
    int last_cnt;
    struct dns_conv_ip *new_dns = NULL;

    last_cnt = g_dns_info.cnt;

    if (++g_dns_info.cnt >= MAX_DNS_CONV_INFO) {
        g_dns_info.cnt = last_cnt;
        port_printf("%s(), dns info queue full!\r\n", __func__);
        return;
    }

    new_dns = &g_dns_info.conv_ip[last_cnt];
    memset(new_dns, 0, sizeof(struct dns_conv_ip));
    memcpy(new_dns, _dns_info, sizeof(struct dns_conv_ip));

    port_printf("%s(), id:%d, name:%s, IP:%s\r\n", __func__,\
        last_cnt, new_dns->dns_name, inet_ntoa(new_dns->ip_addr));
}

static void get_dns_info(int _id, struct sockaddr_in *_server)
{
    struct dns_conv_ip *curr_dns = NULL;

    curr_dns = &g_dns_info.conv_ip[_id];

    memcpy(&_server->sin_addr, &curr_dns->ip_addr, sizeof(struct in_addr));
}

static void del_dns_info(int _id)
{
    int i, j;
    struct dns_info dns_bakup;

    port_printf("%s(), delete dns id:%d, name:%s, IP:%s\r\n", __func__, \
        _id, g_dns_info.conv_ip[_id].dns_name, inet_ntoa(g_dns_info.conv_ip[_id].ip_addr));

    memset(&dns_bakup, 0, sizeof(struct dns_info));
    memcpy(&dns_bakup, &g_dns_info, sizeof(struct dns_info));
    memset(&g_dns_info, 0, sizeof(struct dns_info));

    for (i = 0, j = 0; i < MAX_DNS_CONV_INFO; i++) {
        if ((i == _id) || (!dns_bakup.conv_ip[i].enable))
            continue;

        memcpy(&g_dns_info.conv_ip[j++], &dns_bakup.conv_ip[i], sizeof(struct dns_conv_ip));
        g_dns_info.cnt++;
    }
}

static void increase_fail_cnt(int _id)
{
    struct dns_conv_ip *curr_dns = NULL;

    curr_dns = &g_dns_info.conv_ip[_id];

    if (++curr_dns->fail_cnt > MAX_DNS_CONV_FAIL_CNT) {
        del_dns_info(_id);
    }
}

static int find_dns_info(char *_dns)
{
    int i, max_cnt;
    struct dns_conv_ip *curr_dns = NULL;

    if (g_dns_info.cnt >= MAX_DNS_CONV_INFO) {
        max_cnt = MAX_DNS_CONV_INFO;
    } else {
        max_cnt = g_dns_info.cnt;
    }

    for (i = 0; i < MAX_DNS_CONV_INFO; i++) {
        curr_dns = &g_dns_info.conv_ip[i];

        if (!curr_dns->enable)
            continue;

        if (strncmp(curr_dns->dns_name, _dns, strlen((const char*)_dns)) == 0) {
            break;
        }
    }

    if (i >= max_cnt)
        return -1;

    return i;
}

//---------------------------------------------------------------------------------
// Function Name  : dns_conv_ip
// Description    :
//---------------------------------------------------------------------------------
int dns_conv_ip(char *_addr, struct sockaddr_in *_server)
{
    struct addrinfo *result = NULL;
    struct addrinfo hints;
    int error = -1;

    memset(&hints, 0, sizeof(struct addrinfo)); // make sure the struct is empty
    hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me

    error = getaddrinfo(_addr, NULL, &hints, &result);
    if (error != 0) {
        port_printf("getaddrinfo: %s\n", gai_strerror(error));
        return -1;
    }

    memcpy(_server, (struct sockaddr_in*)result->ai_addr, sizeof(struct sockaddr_in));
    port_printf("%s() get IP address is %s \r\n", __func__, inet_ntoa(_server->sin_addr));

    freeaddrinfo(result);           /* No longer needed */

    return 0;
}

//---------------------------------------------------------------------------------
// Function Name  : dns_conv_ip_nslookup
// Description    :
//---------------------------------------------------------------------------------
int dns_conv_ip_nslookup(char *_addr, struct sockaddr_in *_server)
{
    FILE *fd;
    int read_size = -1;
    unsigned long addr;
    char dns_query_buf[300] = {0,};
    char read_buf[50] = {0,};

    sprintf(dns_query_buf, DNS_QUERY_CMD_STR, _addr, DNS_QUERY_FILE);

    port_printf("<%s> nslookup cmd:%s", __func__, dns_query_buf);

    system(dns_query_buf);

    fd = fopen(DNS_QUERY_FILE, "rb");
    if (fd != NULL) {
        read_size = fread(read_buf, sizeof(char), 50, fd);
        if (read_size < 0) {
            port_printf("%s() file read fail\r\n", __func__);
            return -1;
        }

        read_buf[read_size - 1] = '\0'; // 마지막 문자는 \n 이므로.
        if (strncmp("127.0.0.1", read_buf, strlen("127.0.0.1")) == 0) {
            port_printf("%s() nameserver error : %s\r\n", __func__, read_buf);
            system("udhcpc -i wlan0 -A 1 -b");
            return -1;
        }
        addr = inet_addr(read_buf);
        if (addr == INADDR_NONE) {
            port_printf("%s() inet_addr fail\r\n", __func__);
            return -1;
        }

        _server->sin_addr.s_addr = addr;
    } else {
        port_printf("%s() file open fail\r\n", __func__);
        return -1;
    }

    port_printf("%s() get IP address is %s \r\n", __func__, inet_ntoa(_server->sin_addr));

    return 0;
}

//----------------------------------------------------------------------------
// Function Name  : tcp_connect()
// Description    :
//----------------------------------------------------------------------------
int tcp_connect(char *_addr, char *_port)
{
    struct sockaddr_in server;
    struct dns_conv_ip new_dns_info;
    unsigned long addr;
    int sock = -1;
    int id = -1;    

    port_printf("<%s> %s : %s", __func__, _addr, _port);
    
    addr = inet_addr(_addr);
    if (addr != INADDR_NONE) {  // Numeric IP Address
        port_printf("addr_type is IP \n\r");
        server.sin_addr.s_addr = inet_addr(_addr);
    } else {
        port_printf("addr_type is DNS \n\r");

        id = find_dns_info(_addr);
        if (id >= 0) {
            port_printf("%s() find_dns_info, id : %d\r\n", __func__, id);
            get_dns_info(id, &server);
        } else {
            memset(&new_dns_info, 0, sizeof(struct dns_conv_ip));
            if (dns_conv_ip(_addr, &server) < 0) {
                if (dns_conv_ip_nslookup(_addr, &server) < 0) {
                    port_printf("%s() dns convert all fail\r\n", __func__);
                    return -1;
                }
            }

            new_dns_info.enable = 1;
            new_dns_info.fail_cnt = 0;
            memcpy(new_dns_info.dns_name, _addr, strlen(_addr));
            memcpy(&new_dns_info.ip_addr, &server.sin_addr, sizeof(struct in_addr));
            set_dns_info(&new_dns_info);
        }

        port_printf("%s() get ip:%s\r\n", __func__, inet_ntoa(server.sin_addr));
    }

    server.sin_family = AF_INET;
    server.sin_port = htons(atoi(_port));
    sock = socket(server.sin_family, SOCK_STREAM, IPPROTO_TCP);
    if (sock == -1) {
        port_printf("Could not create socket");
        return -1;
    }

    port_printf("socket create:%d\r\n", sock);

    if (connect_wait(sock, (struct sockaddr*)&server, sizeof(struct sockaddr_in), MAX_CONNECTION_TIMEOUT) < 0) {
        port_printf("can't connect to the server\n");
        if (id > 0) {
            increase_fail_cnt(id);
        }
        tcp_close(sock);
        return -1;
    }

    port_printf("connect to the server\n");

    return sock;
}

//----------------------------------------------------------------------------
// Function Name  :
// Description    :
//----------------------------------------------------------------------------
int send_bytes(int _sock, char *_ptr, int _send_size)
{
    int res_size, send_size, total_size;

    total_size = _send_size;
    send_size = 0;
    do {
        res_size = write(_sock, _ptr + send_size , total_size - send_size);
        if (res_size < 0) {
            port_printf("%s(), Send failed \r\n", __func__);
            return -1;
        }

        if (res_size == 0)
            break;

        send_size += res_size;
    } while (send_size < total_size);

    return send_size;
}

int tcp_send_data(int _sock, char *_data, int _size)
{
    int res_size, send_size, total_size;
    int retry_cnt = 0, i;
    struct timeval tv;
    
    tv.tv_sec  = MAX_CONNECTION_TIMEOUT;
    tv.tv_usec = 0;
    setsockopt(_sock, SOL_SOCKET, SO_SNDTIMEO, (struct timeval *)&tv, sizeof(struct timeval));

    total_size = _size;

    if (total_size > MAX_SEND_PKT_SIZE) {
        retry_cnt = _size / MAX_SEND_PKT_SIZE;
    }

    for (i = 0; i <= retry_cnt; i++) {
        if (total_size > MAX_SEND_PKT_SIZE)
            send_size = MAX_SEND_PKT_SIZE;
        else
            send_size = total_size;

        res_size = send_bytes(_sock, _data,  send_size);
        total_size -= res_size;
    }

    if (total_size)
        return -1;

    return 1;
}

//----------------------------------------------------------------------------
// Function Name  :
// Description    :
//----------------------------------------------------------------------------
int read_bytes(int _sock, char *_ptr, int _total_size)
{
    int cal_total_size, read_size;

    cal_total_size = _total_size;
    
    while (cal_total_size > 0) {
    
        read_size = read(_sock, _ptr, cal_total_size);
        
        if (read_size < 0) {
            return read_size;
        } else {
            if (read_size == 0)
                break;
        }
        cal_total_size -= read_size;
        
        _ptr += read_size;
    }

    return (_total_size - cal_total_size);
}


int tcp_rcv_data(int _sock, char* _buf, int _max_size, int _time_out)
{
    int read_size = 0;
    int bytes_wait;
    fd_set fdset;
    struct timeval tv;

    //i = _max_size;

    FD_ZERO(&fdset);
    FD_SET(_sock, &fdset);

    tv.tv_sec  = _time_out;  
    tv.tv_usec = 0;

    if (select(_sock + 1, &fdset, NULL, NULL, &tv) > 0) {
        ioctl(_sock, FIONREAD, &bytes_wait);

        port_printf("tcp_rcv_data(), ioctl->bytes_wait :%d \r\n", bytes_wait);

        if (bytes_wait > 0) {
            if (_max_size < bytes_wait)
                bytes_wait = _max_size;

            read_size = read_bytes(_sock, _buf, bytes_wait);
            _buf[read_size++] = '\0';
            port_printf("tcp_rcv_data(), read_data :%s , rval :%d\r\n", _buf, read_size);
        }
    }

    return read_size;
}

//----------------------------------------------------------------------------
// Function Name  : tcp_close
// Description    :
//----------------------------------------------------------------------------
int tcp_close(int _sock)
{
    int rval = 1;

    port_printf("tcp_close :%d \r\n", _sock);

    // Gracefully close it
    shutdown(_sock, 0x01);

    close(_sock);

    return rval;
}


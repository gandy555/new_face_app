#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>

#include "port.h"
#include "tcp_api.h"
#include "ftp_api.h"

//----------------------------------------------------------------------------
// Define
//----------------------------------------------------------------------------

#define MAX_BUFFER_SIZE 1024 
#define CMD_BUFFER_SIZE 512 
#define FILE_BUFFER_SIZE 1 * 1024 * 1024 
#define MAX_RESPONSE_TIME 10 //sec

int g_data_socket;

//---------------------------------------------------------------------------------
// Function Name  : get_token()
// Description    :
//---------------------------------------------------------------------------------
static void get_token(char *_token, char *_str_data, int _idx)
{
    char *p = _token;
    char *p2 = _str_data;

    while (_idx > 0) {
        // skip precedent token
        while(*p2 && *p2 != ' ' && *p2 != '\r' && *p2 != '\n') {
            p2++;
        }

        // skip a blank
        p2++;
        _idx--;
    }

    // copy the token
    while (*p2 && *p2 != ' ' && *p2 != '\r' && *p2 != '\n') {
        *p++ = *p2++;
    }

    // add null character to specify the end of string
    *p = '\0'; 
}

//---------------------------------------------------------------------------------
// Function Name  : download()
// Description    :
//---------------------------------------------------------------------------------
static int download(int _sock, ftp_file_info_t *_info, int _size)
{
    char read_buffer[FILE_BUFFER_SIZE];
    char file_path[128] = {0,};
    int read_bytes, total_bytes;
    int fd;

    strcpy(file_path, _info->path);
    strcat(file_path, _info->file_name);
    port_printf("<%s> path: %s", __func__, file_path);
    fd = open(file_path, O_WRONLY | O_CREAT, 0744);
    total_bytes = 0;
    while (total_bytes < _size) {
        //if ((read_bytes = read(_sock, read_buffer, FILE_BUFFER_SIZE)) <= 0) {
        if (tcp_rcv_data(_sock, read_buffer, FILE_BUFFER_SIZE, 3) <= 0) {
            close(fd);
            return total_bytes;
        }
        write(fd, read_buffer, read_bytes);
        total_bytes += read_bytes;
    }

    close(fd);

    return total_bytes;
}

//---------------------------------------------------------------------------------
// Function Name  : upload()
// Description    :
//---------------------------------------------------------------------------------
static int upload(int _data_sock, ftp_file_info_t *_info, int _seek_size)
{
    char read_buffer[FILE_BUFFER_SIZE] = {0,};
    char fullpath[64] = {0,};
    int read_bytes;
    int total_send_size = 0;
    int fd;

    sprintf(fullpath, "%s/%s", _info->path, _info->file_name);
    fd = open(fullpath, O_RDONLY);
    if (fd < 0) {
        port_printf("<%s> open failure!!", __func__);
        return 0;
    }
    
    if (_seek_size > 0) {
        lseek(fd, _seek_size, SEEK_SET);
        total_send_size += _seek_size;
    }

    while ((read_bytes = read(fd, read_buffer, FILE_BUFFER_SIZE)) > 0) {
        if (tcp_send_data(_data_sock, read_buffer, read_bytes) < 0) {
            port_printf("upload_res> send fail!!!, total_send_size:%d\r\n", total_send_size);
            close(fd);
            return total_send_size;
        }

        total_send_size += read_bytes;

        port_printf("upload_res> total_send_size:%d\r\n", total_send_size);

        usleep(1000);
    }

    close(fd);

    return total_send_size;
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_rcv_data()
// Description    :
//---------------------------------------------------------------------------------
static int ftp_rcv_data(int _sock, char *_rcv_data, int _max_size, int _time_out)
{
    if (tcp_rcv_data(_sock, _rcv_data, _max_size, _time_out) <= 0) {
        port_printf("rcv failed\r\n");
        return -1;
    }

    port_printf("ftp rcv : %s\r\n", _rcv_data);

    return 0;
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_send_data()
// Description    :
//---------------------------------------------------------------------------------
static int ftp_send_data(int _sock, char *_protocol)
{
    if (tcp_send_data(_sock, _protocol, strlen(_protocol)) < 0) {
        port_printf("send failed\r\n");
        return -1;
    }

    port_printf("ftp send : %s\r\n", _protocol);

    return 0;
}

//---------------------------------------------------------------------------------
// Function Name  : check_server_response()
// Description    :
//---------------------------------------------------------------------------------
static int check_server_response(int _sock, int res_code, char *_read_data)
{
    char read_buffer[CMD_BUFFER_SIZE] = {0,};
    char read_code[10] = {0,};

    if (ftp_rcv_data(_sock, read_buffer, CMD_BUFFER_SIZE, MAX_RESPONSE_TIME) < 0) {
        port_printf("server not response!!!\r\n");
        return -1;
    }

    get_token(read_code, read_buffer, 0);

    if (res_code == atoi(read_code)) {
        strncpy(_read_data, read_buffer, strlen(read_buffer)+1);
    } else {
        port_printf("It is not equal to the requested code value.\r\n");
        return -1;
    }
    
    return 0;
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_connect
// Description    :
//---------------------------------------------------------------------------------
int ftp_connect(char *_ip, char *_port, char *_user_name, char *_pass)
{
    int sock;
    char rcv_data[CMD_BUFFER_SIZE] = {0,};

    sock = tcp_connect(_ip, _port);
    if (sock < 0) {
        port_printf("FTP server connect fail!!\r\n");
        return -1;
    }
    
    if (check_server_response(sock, 220, rcv_data) < 0) {
        port_printf("connect fail\r\n");
        return -1;
    }

    if (_user_name != NULL) {
        ftp_login(sock, _user_name, _pass);
    }
    
    return sock;
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_close
// Description    :
//---------------------------------------------------------------------------------
int ftp_close(int _sock)
{
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};

    memset(send_data, 0, CMD_BUFFER_SIZE);
    sprintf(send_data, "QUIT\r\n");
    ftp_send_data(_sock, send_data);

    check_server_response(_sock, 221, rcv_data);

    tcp_close(_sock);

    return 0;
}


//---------------------------------------------------------------------------------
// Function Name  : ftp_login
// Description    :
//---------------------------------------------------------------------------------
int ftp_login(int _sock, char *_user, char *_pass)
{
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};

    sprintf(send_data, "USER %s\r\n", _user);
    ftp_send_data(_sock, send_data);
    if (check_server_response(_sock, 331, rcv_data) < 0) {
        port_printf("User name is incorrect!!\r\n");
        return -1;
    }

    memset(send_data, 0, CMD_BUFFER_SIZE);
    memset(rcv_data, 0, CMD_BUFFER_SIZE);
    sprintf(send_data, "PASS %s\r\n", _pass);
    ftp_send_data(_sock, send_data);
    if (check_server_response(_sock, 230, rcv_data) < 0) {
        port_printf("Password is incorrect!!\r\n");
        return -1;
    }

    return 0;
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_cwd
// Description    :
//---------------------------------------------------------------------------------
int ftp_cwd(int _sock, char *_root_path)
{
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};

    sprintf(send_data, "CWD %s\r\n", _root_path);
    ftp_send_data(_sock, send_data);
    if (check_server_response(_sock, 250, rcv_data) < 0) {
        return -1;
    }

    return 0;
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_passive_mode
// Description    :
//---------------------------------------------------------------------------------
int ftp_passive_mode(int _sock, char *_ip, char *_port)
{
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};
	int host0, host1, host2, host3;
	int port0, port1;

    sprintf(send_data, "PASV\r\n");
    ftp_send_data(_sock, send_data);
    if (check_server_response(_sock, 227, rcv_data) < 0) {
        return -1;
    }

	sscanf(strchr(rcv_data, '(')+1, "%d,%d,%d,%d,%d,%d", &host0, &host1, &host2, &host3, &port0, &port1);
	sprintf(_ip, "%d.%d.%d.%d", host0, host1, host2, host3);
  	sprintf(_port, "%d", (port0*256 + port1));

    return 0;
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_rest
// Description    :
//---------------------------------------------------------------------------------
int ftp_rest(int _sock, int _trans_size)
{
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};

    sprintf(send_data, "REST %d\r\n", _trans_size);
    ftp_send_data(_sock, send_data);
    if (check_server_response(_sock, 350, rcv_data) < 0) {
        return -1;
    }

    return 0;
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_list
// Description    :
//---------------------------------------------------------------------------------
/*
int ftp_list(int _sock, char *_file_name)
{
    int data_socket;
    char ip[128] = {0,};
    char port[16] = {0,};
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};

    if (ftp_passive_mode(_sock, ip, port) < 0) {
        return -1;
    }

    port_printf("ip:%s, port:%s\r\n", ip, port);

    data_socket = tcp_connect(ip, port);
    if (data_socket < 0) {
        printf("FTP server data port connect fail!!\r\n");
        return -1;
    }

    sprintf(send_data, "LIST %s\r\n", _file_name);
    ftp_send_data(_sock, send_data);

    if (check_server_response(_sock, 150, rcv_data) < 0) {
        return -1;
    }

    memset(rcv_data, 0, CMD_BUFFER_SIZE);
    if (ftp_rcv_data(data_socket, rcv_data, CMD_BUFFER_SIZE, MAX_RESPONSE_TIME) < 0) {
        printf("server not response!!!\r\n");
        tcp_close(data_socket);
        return -1;
    }

    if (check_server_response(_sock, 226, rcv_data) < 0) {
        return -1;
    }

    tcp_close(data_socket);

    return 0;
}
*/

//---------------------------------------------------------------------------------
// Function Name  : ftp_file_size
// Description    :
//---------------------------------------------------------------------------------
int ftp_file_size(int _sock, char *_file_name)
{
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};
    char file_size_str[CMD_BUFFER_SIZE] = {0,};
    int file_size = 0;

    sprintf(send_data, "SIZE %s\r\n", _file_name);
    ftp_send_data(_sock, send_data);

    if (check_server_response(_sock, 213, rcv_data) < 0) {
        return file_size;
    }

    get_token(file_size_str, rcv_data, 1);
    file_size = atoi(file_size_str);

    //port_printf("ftp_file_size  : %d\r\n", file_size);

    return file_size;
}


//---------------------------------------------------------------------------------
// Function Name  : ftp_file_upload
// Description    :
//---------------------------------------------------------------------------------
int ftp_file_upload(int _sock, ftp_file_info_t *_info)
{
    int fail_cnt = 0;
    int data_socket;
    int upload_size = 0, canceled_size;
    char ip[128] = {0,};
    char port[16] = {0,};
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};

    while (1) {
        canceled_size = ftp_file_size(_sock, _info->file_name);
        if (ftp_passive_mode(_sock, ip, port) < 0) {
            return -1;
        }

        port_printf("ip:%s, port:%s\r\n", ip, port);

        data_socket = tcp_connect(ip, port);
        if (data_socket < 0) {
            port_printf("FTP server data port connect fail!!\r\n");
            return -1;
        }

        if (canceled_size > 0) {
            ftp_rest(_sock, canceled_size);
        }

        sprintf(send_data, "STOR %s\r\n", _info->file_name);
        ftp_send_data(_sock, send_data);
        if (check_server_response(_sock, 150, rcv_data) < 0) {
            return -1;
        }

        upload_size = upload(data_socket, _info, canceled_size);

        port_printf("upload_size:%d, file_size:%d\r\n", upload_size, _info->size);

        tcp_close(data_socket);

        memset(rcv_data, 0, CMD_BUFFER_SIZE);
        if (check_server_response(_sock, 226, rcv_data) < 0) {
            if (upload_size != _info->size)
                return -1;
        }
        //ftp_rcv_data(_sock, rcv_data, CMD_BUFFER_SIZE, 10);

        if (upload_size != _info->size) {
            if (fail_cnt++ > 5)
                return -1;
            else
                continue;
        }

        return 0;
    }
}

//---------------------------------------------------------------------------------
// Function Name  : ftp_file_download
// Description    :
//---------------------------------------------------------------------------------
int ftp_file_download(int _sock, ftp_file_info_t *_info)
{
    int fail_cnt = 0;
    int data_socket;
    int file_size = 0;
    char ip[128] = {0,};
    char port[16] = {0,};
    char rcv_data[CMD_BUFFER_SIZE] = {0,};
    char send_data[CMD_BUFFER_SIZE] = {0,};

    while (1) {
        if (ftp_passive_mode(_sock, ip, port) < 0) {
            return -1;
        }

        port_printf("ip:%s, port:%s\r\n", ip, port);

        data_socket = tcp_connect(ip, port);
        if (data_socket < 0) {
            port_printf("FTP server data port connect fail!!\r\n");
            return -1;
        }

        sprintf(send_data, "RETR %s\r\n", _info->file_name);
        ftp_send_data(_sock, send_data);
        if (check_server_response(_sock, 150, rcv_data) < 0) {
            return -1;
        }

        // extract fileSize 
        sscanf(strchr(rcv_data, '(')+1, "%u", &file_size);
        port_printf("<%s> fileSize: %u", __func__, file_size);

        download(data_socket, _info, file_size);

        port_printf("download_size:%d, file_size:%d\r\n", file_size, _info->size);

        tcp_close(data_socket);

        memset(rcv_data, 0, CMD_BUFFER_SIZE);
        if (check_server_response(_sock, 226, rcv_data) < 0) {
            if (file_size != _info->size)
                return -1;
        }
        //ftp_rcv_data(_sock, rcv_data, CMD_BUFFER_SIZE, 10);

        if (file_size != _info->size) {
            if (fail_cnt++ > 5)
                return -1;
            else
                continue;
        }

        return 0;
    }
}



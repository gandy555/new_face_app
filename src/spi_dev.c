#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include "port.h"

char g_txbuf[32] = {0,};
char g_rxbuf[32] = {0,};
 
struct spi_ioc_transfer g_xfer[2];
 
int spi_init(char *filename)
{
    int file;
    __u8 mode, lsb, bits;
    __u32 speed=2500000;

    if ((file = open(filename,O_RDWR)) < 0) {
        printf("Failed to open the bus.");
        return -1;
    }

    #if 0
    // SPI_LOOP, SPI_CPHA, SPI_CPOL, SPI_LSB_FIRST,
    // SPI_CS_HIGH, SPI_3WIRE, SPI_NO_CS, SPI_READY;
    if (ioctl(file, SPI_IOC_WR_MODE, &mode) < 0) {
        port_printf("ERROR OCCURED: SPI_IOC_WR_MODE");
        return -1;
    }
    #endif
    
    if (ioctl(file, SPI_IOC_RD_MODE, &mode) < 0) {
        port_printf("ERROR OCCURED: SPI_IOC_RD_MODE");
        return -1;
    }
    
    if (ioctl(file, SPI_IOC_RD_LSB_FIRST, &lsb) < 0) {
        port_printf("ERROR OCCURED: SPI_IOC_RD_LSB_FIRST");
        return -1;
    }

    // if you want to word configuration the as below
    #if 0   
    if (ioctl(file, SPI_IOC_WR_BITS_PER_WORD, (__u8[1]){8})<0) {
        port_printf("can't set bits per word");
        return -1;
    }
    #endif
    
    if (ioctl(file, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0) {
        port_printf("ERROR OCCURED: SPI_IOC_RD_BITS_PER_WORD");
        return -1;
    }

    #if 0
    if (ioctl(file, SPI_IOC_WR_MAX_SPEED_HZ, &speed)<0)  {
        perror("can't set max speed hz");
        return -1;
    }
    #endif
    
    if (ioctl(file, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0) {
        port_printf("ERROR OCCURED: SPI_IOC_RD_MAX_SPEED_HZ");
        return -1;
    }

    port_printf("%s: spi mode %d, %d bits %sper word, %d Hz max\n",
            filename, mode, bits, lsb ? "(lsb first) " : "", speed);

    //g_xfer[0].tx_buf = (unsigned long)g_txbuf;
    g_xfer[0].len = 3; /* Length of  command to write*/
    g_xfer[0].cs_change = 0; /* Keep CS activated */
    g_xfer[0].delay_usecs = 0, //delay in us
    g_xfer[0].speed_hz = 2500000, //speed
    g_xfer[0].bits_per_word = 8, // bites per word 8

    //g_xfer[1].rx_buf = (unsigned long) g_rxbuf;
    g_xfer[1].len = 4; /* Length of Data to read */
    g_xfer[1].cs_change = 0; /* Keep CS activated */
    g_xfer[0].delay_usecs = 0;
    g_xfer[0].speed_hz = 2500000;
    g_xfer[0].bits_per_word = 8;

    return file;
}
 
int spi_read(int add1, int add2, int nbytes, char *buffer, int file)
{
    int status;

    memset(g_txbuf, 0, sizeof(g_txbuf));
    memset(g_rxbuf, 0, sizeof(g_rxbuf));

    g_txbuf[0] = 0x01;
    g_txbuf[1] = add1;
    g_txbuf[2] = add2;
    g_xfer[0].tx_buf = (unsigned long)g_txbuf;
    g_xfer[0].len = 3; /* Length of  command to write*/
    g_xfer[1].rx_buf = (unsigned long)g_rxbuf;
    g_xfer[1].len = nbytes; /* Length of Data to read */
    status = ioctl(file, SPI_IOC_MESSAGE(2), g_xfer);
    if (status < 0) {
        port_printf("ERROR OCCURED: SPI_IOC_MESSAGE(RD)");
        return -1;
    }
    
    //port_printf("env: %02x %02x %02x\n", g_txbuf[0], g_txbuf[1], g_txbuf[2]);
    //port_printf("ret: %02x %02x %02x %02x\n", buffer[0], buffer[1], buffer[2], buffer[3]);

    memcpy(buffer, g_rxbuf, nbytes);
    
    return nbytes;
}
 
int spi_write(int add1, int add2, int nbytes, char *value, int file)
{
    int status;

    memset(g_txbuf, 0, sizeof(g_txbuf));
    memset(g_rxbuf, 0, sizeof(g_rxbuf));

    g_txbuf[0] = 0x00;
    g_txbuf[1] = add1;
    g_txbuf[2] = add2;
    if (nbytes>=1) g_txbuf[3] = value[0];
    if (nbytes>=2) g_txbuf[4] = value[1];
    if (nbytes>=3) g_txbuf[5] = value[2];
    if (nbytes>=4) g_txbuf[6] = value[3];
    g_xfer[0].tx_buf = (unsigned long)g_txbuf;
    g_xfer[0].len = nbytes+3; /* Length of  command to write*/
    status = ioctl(file, SPI_IOC_MESSAGE(1), g_xfer);
    if (status < 0) {
        port_printf("ERROR OCCURED: SPI_IOC_MESSAGE(WR)");
        return -1;
    }

    //port_printf("env: %02x %02x %02x\n", g_txbuf[0], g_txbuf[1], g_txbuf[2]);
    //port_printf("ret: %02x %02x %02x %02x\n", g_rxbuf[0], g_rxbuf[1], g_rxbuf[2], g_rxbuf[3]);

    return 0;
}


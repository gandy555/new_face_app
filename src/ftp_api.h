#ifndef __FTP_API_H__
#define __FTP_API_H__

//----------------------------------------------------------------------------
// Define
//----------------------------------------------------------------------------
typedef struct {
    int size;
    char path[64];
    char file_name[64];
} ftp_file_info_t;

//----------------------------------------------------------------------------
// export functions
//----------------------------------------------------------------------------
extern int ftp_connect(char *_ip, char *_port, char *_user_name, char *_pass);
extern int ftp_close(int _sock);
extern int ftp_login(int _sock, char *_user, char *_pass);
extern int ftp_cwd(int _sock, char *_root_path);
extern int ftp_passive_mode(int _sock, char *_ip, char *_port);
extern int ftp_rest(int _sock, int _trans_size);
extern int ftp_file_upload(int _sock, ftp_file_info_t *_info);
extern int ftp_file_download(int _sock, ftp_file_info_t *_info);
#endif // __FTP_API_H__
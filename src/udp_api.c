#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>

#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h> 
#include <syslog.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <getopt.h>

#include "port.h"
#include "udp_api.h"

struct sockaddr_in g_server_addr;

int udp_server_start(char *_port)
{
    int rval = 1;
    int sock = -1;
    int socket_type;
    struct sockaddr_in server_addr;

    //Create socket
    sock = socket(AF_INET, SOCK_DGRAM , 0);
    if (sock == -1) {
        port_printf("Could not create socket");
        return -1;
    }
    port_printf("Socket created : %d\n", sock);    

    socket_type = 1;
    rval = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &socket_type, sizeof(socket_type)); // SO_BROADCAST
    if (rval < 0) {
        port_printf("UDP setsockopt failed, rval :%d\n", rval);
        udp_close(sock);
        return -1;
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);    //INADDR_BROADCAST
    server_addr.sin_port = htons(atoi((const char*)_port));

    /* 소켓에 주소 정보 연결 */
    rval = bind(sock, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if (rval < 0) {
       port_printf("UDP bind failed , rval :%d\n", rval);
       udp_close(sock);
       return -1;
    }

    return sock;
}

int udp_client_start(char *_addr, char *_port)
{
    int sock = -1;

    //Create socket
    sock = socket(AF_INET, SOCK_DGRAM , 0);
    if (sock == -1) {
        port_printf("Could not create socket");
        return -1;
    }
    port_printf("Socket created : %d\n", sock);    

    g_server_addr.sin_family = AF_INET;
    g_server_addr.sin_addr.s_addr = inet_addr(_addr); //htonl(INADDR_ANY); 
    g_server_addr.sin_port = htons(atoi((const char*)_port));

    return sock;
}

int udp_send_data(int _sock, char* _buf, int _size)
{
    int res = sendto(_sock, _buf, _size, 0,
        (struct sockaddr *)&g_server_addr, sizeof(struct sockaddr_in));
    if (res < 0) {
        port_printf("<%s> fail", __func__);
    }

    return res;
}

int udp_rcv_data(int _sock, char* _buf, int _max_size, int _time_out)
{
    fd_set fdset;
    int rval = 0;
    int client_addr_size = sizeof(struct sockaddr_in);
    struct timeval tv;
    struct sockaddr_in client_addr;

    FD_ZERO(&fdset);
    FD_SET(_sock, &fdset);

    tv.tv_sec  = _time_out;  
    tv.tv_usec = 0;

    port_printf("udp_rcv_data(), Socket : %d\n", _sock);

    if (select(_sock + 1, &fdset, NULL, NULL, &tv) > 0) {
        rval = (int)recvfrom(_sock, _buf, _max_size, 0, (struct sockaddr*)&client_addr, (socklen_t *)&client_addr_size);
        if (rval < 0) {
            // failed
            port_printf("UDP select failed!!, rval:%d \r\n", rval);
        } else if (rval > 0) {
            _buf[rval++] = '\0';

            port_printf("Recevied: %s, size : %d \r\n", _buf, rval);
        }
    } else {
        // No response
        port_printf("UDP select timeout!!, rval:%d \r\n", rval);
    }

    return rval;
}

int udp_close(int _sock)
{
    int rval = 1;

    port_printf("udp_close : %d \r\n", _sock);

    close(_sock);

    return rval;
}


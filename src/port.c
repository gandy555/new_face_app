﻿/******************************************************************************
 * Filename:
 *   port_linux.c
 *
 * Description:
 *   platform porting layer (OS Abstract Layer)
 *
 * Author:
 *   gandy
 *
 * Version : V0.1_19-11-12
 * ---------------------------------------------------------------------------
 * Abbreviation
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/time.h>
#include <sys/ipc.h>  
#include <sys/msg.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <termios.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/rtc.h>
#include <pthread.h>
#include <sched.h>
#include <sys/sendfile.h>

#include "port.h"
/******************************************************************************
 *
 * Variable Declaration
 *
 ******************************************************************************/
//#define PLAT_ANDROID_LINUX
#if defined(PLAT_ANDROID_LINUX)
#include <android/log.h>
#endif

struct evflag {
    pthread_cond_t var;
    pthread_mutex_t mtx;
    uint32_t flags;
};

static key_t s_mq_key = 4500;

char * col_str[CO_MAX] = {
    "\033[0;31;1m", /* RED      */
    "\033[0;32;1m", /* GREEN    */
    "\033[0;33;1m", /* BROWN    */
    "\033[0;34;1m", /* BLUE         */
    "\033[0;35;1m", /* PURPLE   */
    "\033[0;36;7m", /* CYAN         */
    "\033[0m"       /* DEFAULT  */
};

#define SECS_PER_MIN		(60)
#define MINS_PER_HOUR		(60)
#define HOURS_PER_DAY		(24)
#define DAYS_PER_WEEK		(7)
#define DAYS_PER_NYEAR		(365)
#define DAYS_PER_LYEAR		(366)
#define SECS_PER_HOUR		(3600)	/* (SECS_PER_MIN * MINS_PER_HOUR) */
#define SECS_PER_DAY		(86400)	/* (SECS_PER_HOUR * HOURS_PER_DAY) */
#define MONS_PER_YEAR		(12)

/** Nonzero if YEAR is a leap year (every 4 years, except every 100th isn't,
 *  and every 400th is).
 */
#define LEAP(year)	\
	((year) % 4 == 0 && ((year) % 100 != 0 || (year) % 400 == 0))

#define DIV(a, b)		((a) / (b) - ((a) % (b) < 0))
#define LEAPS_THRU_END_OF(y)	(DIV (y, 4) - DIV (y, 100) + DIV (y, 400))

static const unsigned short int s_mon_yday[2][13] = {
     /* Normal years.  */
     { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 },
     /* Leap years.  */
     { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366 }
 };
     

/* default vlaue is 9 hour */
static int s_gmt_offset = 9*60*60;
static int s_dst_on = 0;
/* 
    gmt offset dst 등등 타임 관련 변수의 변화가 발생 시 GPS 연결 상태이면, time sync를 유도한다.
*/
static int s_need_time_sync = 1;

#define RTC_DEV             "/dev/rtc0"
static pthread_mutex_t s_rtc_mutex = PTHREAD_MUTEX_INITIALIZER;

#define IPC_MSG_TYPE         100

#if defined(PLAT_ANDROID_LINUX)
typedef struct itc_msg_list {
    port_mq_t *msg_ptr;
    struct itc_msg_list *prev_ptr;
    struct itc_msg_list *next_ptr;
} ITC_MSG_LIST;

#define MAX_MSG_MOD     20

#define ITC_MSG_HEAD   ((ITC_MSG_LIST*)NULL)
#define ITC_MSG_TAIL        ((ITC_MSG_LIST*)NULL)
#define ITC_MSG_EMPTY       ((port_mq_t*)NULL)
#define ITC_BUF_EMPTY       ((void*)NULL)
#define ITC_MSG_SIZE        sizeof(port_mq_t)
#define ITC_MSG_LIST_SIZE   sizeof(ITC_MSG_LIST)


#define MAX_BUF_SIZE        256
//#define DELAY_MS          (100)   // Delay in milliseconds
#define DELAY_MS            (5) // Delay in milliseconds
#define WAIT_INFINITE       -1
#define WAIT_MAX_TIME_OUT   0xffffff

#define itc_msg_list_malloc(sIZE_t)       (ITC_MSG_LIST*)malloc(sIZE_t)
#define itc_msg_list_free(pTR)            if (pTR) free(pTR)
#define itc_msg_malloc(sZIE_t)            (port_mq_t*)malloc(sZIE_t)
#define itc_msg_free(pTR)                 if (pTR) free(pTR)
#define itc_buf_malloc(sZIE_t)            (void*) malloc(sZIE_t)
#define itc_buf_free(pTR)                 if (pTR) free(pTR)

pthread_mutex_t msg_access_sync;
int g_msg_init_done = 0;
static int g_msg_used_list[MAX_MSG_MOD];
ITC_MSG_LIST* g_itc_msg_list[MAX_MSG_MOD];
#else
#define IPC_MSG_SIZE     (sizeof(port_mq_t)-sizeof(long))
#endif


#if defined(PLAT_ANDROID_LINUX)
static ITC_MSG_LIST* msg_find_id(int _mod)
{
    ITC_MSG_LIST  *msg_list_ptr;

    pthread_mutex_lock(&msg_access_sync); // synchronization begins
    msg_list_ptr = g_itc_msg_list[_mod];
    pthread_mutex_unlock(&msg_access_sync);   // synchronization ends

    return msg_list_ptr->next_ptr;
}

static ITC_MSG_LIST* msg_find_tail(int _mod)
{
    ITC_MSG_LIST *msg_list_ptr, *tmp_ptr;

    msg_list_ptr = g_itc_msg_list[_mod];
    while (1) {
        if (msg_list_ptr->next_ptr != ITC_MSG_TAIL) {
            tmp_ptr = msg_list_ptr->next_ptr;
            msg_list_ptr = tmp_ptr;
        } else {
            break;
        }
    }

    return msg_list_ptr;
}

static ITC_MSG_LIST* msg_append_queue(int mod, port_mq_t *_msg)
{
    port_mq_t *msg_ptr;
    ITC_MSG_LIST *tail_ptr, *tmp_list_ptr = (ITC_MSG_LIST*)NULL;

    pthread_mutex_lock(&msg_access_sync); // synchronization begins
    tail_ptr = msg_find_tail(mod);
    pthread_mutex_unlock(&msg_access_sync);   // synchronization ends

    if (tail_ptr != (ITC_MSG_LIST*)NULL) {
        pthread_mutex_lock(&msg_access_sync); // synchronization begins
        msg_ptr = itc_msg_malloc(ITC_MSG_SIZE);        
        if (msg_ptr) {
            memset(msg_ptr, 0, ITC_MSG_SIZE);
            msg_ptr->type = _msg->type; 
            msg_ptr->msg_id = _msg->msg_id;
            msg_ptr->param[0] = _msg->param[0];
            msg_ptr->param[1] = _msg->param[1];
        } else {
            return (ITC_MSG_LIST*)NULL;
        }

        tmp_list_ptr = itc_msg_list_malloc(ITC_MSG_LIST_SIZE);
        if (tmp_list_ptr) {
            tmp_list_ptr->msg_ptr = msg_ptr;
            tail_ptr->next_ptr = tmp_list_ptr;
            tmp_list_ptr->prev_ptr = tail_ptr;
            tmp_list_ptr->next_ptr = ITC_MSG_TAIL;
        }
        pthread_mutex_unlock(&msg_access_sync); // synchronization ends
    }

    return tmp_list_ptr;
}

static int msg_remove_queue(ITC_MSG_LIST* _msg_list_ptr)
{
    ITC_MSG_LIST *prev_list_ptr, *next_list_ptr;

    if (_msg_list_ptr) {
        pthread_mutex_lock(&msg_access_sync); // synchronization begins        
        prev_list_ptr = _msg_list_ptr->prev_ptr;
        next_list_ptr = _msg_list_ptr->next_ptr;

        if (next_list_ptr != ITC_MSG_TAIL) {
            prev_list_ptr->next_ptr = next_list_ptr;
            next_list_ptr->prev_ptr = prev_list_ptr;
        } else {
            prev_list_ptr->next_ptr = ITC_MSG_TAIL;
        }

        itc_msg_free(_msg_list_ptr->msg_ptr);
        itc_msg_list_free(_msg_list_ptr);
        pthread_mutex_unlock(&msg_access_sync); // synchronization ends

        return 1;
    }

    return -1;
}

static int msg_create_queue(void)
{
    int i;
    ITC_MSG_LIST *head_ptr;

    if (g_msg_init_done == 0) {
        g_msg_init_done = 1;
        pthread_mutex_init(&msg_access_sync, NULL);
    }
    
    for (i = 0; i < MAX_MSG_MOD; i++) {
        if (g_msg_used_list[i] == 0) {
            head_ptr = itc_msg_list_malloc(ITC_MSG_LIST_SIZE);
            g_itc_msg_list[i] = head_ptr;
            g_itc_msg_list[i]->msg_ptr = ITC_MSG_EMPTY;
            g_itc_msg_list[i]->prev_ptr = ITC_MSG_HEAD;  
            g_itc_msg_list[i]->next_ptr = ITC_MSG_TAIL;

            g_msg_used_list[i] = 1;
            return i;
        } 
    }

    return -1;
}

static int msg_destroy_queue(int _mod)
{
    ITC_MSG_LIST *msg_list_ptr;

    while (1) {
        msg_list_ptr = msg_find_tail(_mod);

        if (msg_list_ptr->prev_ptr == ITC_MSG_HEAD)
            break;
        msg_remove_queue(msg_list_ptr);
    }

    return 1;
}

static int msg_destroy_all_queues(void)
{
    int i;

    for (i = 0; i < (int)MAX_MSG_MOD; i++) {
        msg_destroy_queue(i);
        itc_msg_list_free(g_itc_msg_list[i]);
    }

    return 1;
}   
#endif  // defined(PLAT_ANDROID_LINUX)

//------------------------------------------------------------------------------
// Function Name  : port_ascii2uni()
// Description    :
//------------------------------------------------------------------------------
void port_ascii2uni(const char *ascstr, uint16_t *unistr,  int len)
{
    strncpy((char*)unistr, ascstr, len);
}

//------------------------------------------------------------------------------
// Function Name  : port_mono_time()
// Description    :
//------------------------------------------------------------------------------
uint64_t port_mono_time(void)
{
    struct timespec t;
    static uint64_t start_time = 0;
    uint64_t utime;
    
    if (start_time == 0) {
        clock_gettime(CLOCK_MONOTONIC, &t);
        start_time = (t.tv_sec * 1000) + (t.tv_nsec / 1000 / 1000);
    }
    clock_gettime(CLOCK_MONOTONIC, &t);
    utime = (t.tv_sec * 1000) + (t.tv_nsec / 1000 / 1000);
    utime = utime - start_time;

    return utime;
}

//------------------------------------------------------------------------------
// Function Name  : port_mono_elapsed_time()
// Description    :
//------------------------------------------------------------------------------
uint64_t port_mono_elapsed_time(uint64_t p_time)
{
    uint64_t e_time;

    e_time = port_mono_time() - p_time;

    return e_time;
}

//------------------------------------------------------------------------------
// Function Name  : port_gmtime()
// Description    :
//------------------------------------------------------------------------------
void port_gmtime(const port_time_t *t, port_tm_t *_result)
{   
    struct tm gm_tm;
    time_t time;

    time = *t;
    gmtime_r(&time, &gm_tm);
    _result->tm_sec = gm_tm.tm_sec;
    _result->tm_min = gm_tm.tm_min;
    _result->tm_hour = gm_tm.tm_hour;
    _result->tm_mday = gm_tm.tm_mday;
    _result->tm_mon = gm_tm.tm_mon;
    _result->tm_year = gm_tm.tm_year;
    _result->tm_wday = gm_tm.tm_wday;
    _result->tm_yday = gm_tm.tm_yday;
    _result->tm_isdst = gm_tm.tm_isdst;
}
  
//------------------------------------------------------------------------------
// Function Name  : port_localtime()
// Description    :
//------------------------------------------------------------------------------
void port_localtime(const port_time_t *t, port_tm_t *_result)
{
    port_time_t time;

    time = *t + s_gmt_offset;

    port_gmtime(&time, _result);
}

//------------------------------------------------------------------------------
// Function Name  : port_is_need_time_sync()
// Description    : to check whether time synchronization is required
//------------------------------------------------------------------------------
int port_is_need_time_sync(void)
{
    return s_need_time_sync;
}

//------------------------------------------------------------------------------
// Function Name  : port_set_gmtoffset()
// Description    : minute offset
//------------------------------------------------------------------------------
void port_set_gmtoffset(int sec)
{
    if (s_gmt_offset != sec) {
        port_printf_co(CO_BLUE, "GMT Offset is changed(%d)", sec);
        s_need_time_sync = 1;
    }

    s_gmt_offset = sec;
}

//------------------------------------------------------------------------------
// Function Name  : port_get_gmtoffset()
// Description    : minute offset
//------------------------------------------------------------------------------
int port_get_gmtoffset(void)
{
    return s_gmt_offset;
}

//------------------------------------------------------------------------------
// Function Name  : port_set_time_dst()
// Description    : set the day light saving time
//------------------------------------------------------------------------------
int port_set_time_dst(int dst)
{
    if (s_dst_on != dst) {
        port_printf_co(CO_BLUE, "DST is changed(%d)", dst);
        s_need_time_sync = 1;
    }
    s_dst_on = dst;

    return 1;
}

//------------------------------------------------------------------------------
// Function Name  : port_get_time_dst()
// Description    : get the day light saving time
//------------------------------------------------------------------------------
int port_get_time_dst(void)
{
    return s_dst_on;
}

//------------------------------------------------------------------------------
// Function Name  : port_curr_time()
// Description    :
//------------------------------------------------------------------------------
void port_curr_time(port_tm_t *_loc_time)
{   
    time_t curr_time;
    struct tm loc_tm;
    
    curr_time = time(NULL);
    localtime_r(&curr_time, &loc_tm);
    _loc_time->tm_sec = loc_tm.tm_sec;
    _loc_time->tm_min = loc_tm.tm_min;
    _loc_time->tm_hour = loc_tm.tm_hour;
    _loc_time->tm_mday = loc_tm.tm_mday;
    _loc_time->tm_mon = loc_tm.tm_mon;
    _loc_time->tm_year = loc_tm.tm_year;    
    _loc_time->tm_wday = loc_tm.tm_wday;
}

//------------------------------------------------------------------------------
// Function Name  : port_time_str()
// Description    :
//------------------------------------------------------------------------------
void port_time_str(port_tm_t *_time, char *_str)
{
    #if 0
    static char wday_name[7][3] = {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    static char mon_name[12][3] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    #endif
    
    sprintf(_str, "%d%02d%02d_%.2d_%.2d_%.2d",
        1900 + _time->tm_year, _time->tm_mon+1, _time->tm_mday, 
        _time->tm_hour, _time->tm_min, _time->tm_sec);
}

//------------------------------------------------------------------------------
// Function Name  : port_mktime()
// Description    :
//------------------------------------------------------------------------------
port_time_t port_mktime(port_tm_t *tp)
{
    struct tm curr_tm;
    time_t ret;
    port_time_t res;

    curr_tm.tm_sec = tp->tm_sec;
    curr_tm.tm_min = tp->tm_min;
    curr_tm.tm_hour = tp->tm_hour;
    curr_tm.tm_mday = tp->tm_mday;
    curr_tm.tm_mon = tp->tm_mon;
    curr_tm.tm_year = tp->tm_year;  
    ret = mktime(&curr_tm);
    res = ret;

    return res;
}

//------------------------------------------------------------------------------
// Function Name  : port_tm_to_time()
// Description    : tm 입력을 seconds 단위의 값 으로 변환. offset 무시
//------------------------------------------------------------------------------
port_time_t port_tm_to_time(port_tm_t *tp)
{
    int i, tolday = 0;
    port_time_t rval = 0;

    if (tp->tm_year < 70) {
        port_printf_co(CO_BLUE, "Invalid tm_year: %d", tp->tm_year);
        tp->tm_year = 70;
    }
    
    tolday = 0;
    for (i = 1970; i < (tp->tm_year+1900); i++)
        tolday += (LEAP(i) ? DAYS_PER_LYEAR : DAYS_PER_NYEAR);

    tolday += s_mon_yday[LEAP(tp->tm_year) ? 1 : 0][tp->tm_mon];
    tolday += (tp->tm_mday - 1);

    rval = tolday * SECS_PER_DAY + tp->tm_hour * SECS_PER_HOUR +
        tp->tm_min * SECS_PER_MIN + tp->tm_sec;

    return rval;
}

//------------------------------------------------------------------------------
// Function Name  : port_set_rtc_time()
// Description    :
//------------------------------------------------------------------------------
int port_set_rtc_time(port_tm_t *tp)
{
    struct rtc_time rtc_tm;
    int retval;
    int rtc_fd = -1;
    char time_str[64] = {0,};

    pthread_mutex_lock(&s_rtc_mutex);
    rtc_fd = open(RTC_DEV, O_RDONLY);
    if (rtc_fd < 0)   {
        pthread_mutex_unlock(&s_rtc_mutex);
        port_printf_co(CO_RED, "<%s> open failed: %d", __func__, rtc_fd);
        return -1;
    }

    rtc_tm.tm_year = tp->tm_year;
    rtc_tm.tm_mon = tp->tm_mon;
    rtc_tm.tm_mday = tp->tm_mday;
    rtc_tm.tm_hour = tp->tm_hour;
    rtc_tm.tm_min = tp->tm_min;
    rtc_tm.tm_sec = tp->tm_sec;

    retval = ioctl(rtc_fd, RTC_SET_TIME, &rtc_tm);
    if (retval < 0) {
        close(rtc_fd);
        pthread_mutex_unlock(&s_rtc_mutex);
        port_printf_co(CO_RED, "ioctl RTC_SET_TIME  faild: %d", retval);
        return -2;
    }

    close(rtc_fd);
    s_need_time_sync = 0;
    pthread_mutex_unlock(&s_rtc_mutex);
    
    system("hwclock -s");

    port_time_str(tp, time_str);

    port_printf_co(CO_BLUE, "<%s> %s", __func__, time_str);
    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : port_get_rtc_time()
// Description    :
//------------------------------------------------------------------------------
int port_get_rtc_time(port_tm_t *tp)
{
    struct rtc_time rtc_tm;
    int retval;
    int rtc_fd = -1;

    pthread_mutex_lock(&s_rtc_mutex);
    rtc_fd = open(RTC_DEV, O_RDONLY);
    if (rtc_fd < 0) {
        pthread_mutex_unlock(&s_rtc_mutex);
        port_printf_co(CO_RED, "<%s> open failed: %d", __func__, rtc_fd);
        return -1;
    }

    retval = ioctl(rtc_fd, RTC_RD_TIME, &rtc_tm);
    if (retval < 0) {
        close(rtc_fd);
        pthread_mutex_unlock(&s_rtc_mutex);
        port_printf_co(CO_RED, "ioctl RTC_SET_TIME  faild: %d", retval);
        return -2;
    }
    close(rtc_fd);
    pthread_mutex_unlock(&s_rtc_mutex);
    
    tp->tm_sec = rtc_tm.tm_sec;
    tp->tm_min = rtc_tm.tm_min;
    tp->tm_hour = rtc_tm.tm_hour;
    tp->tm_mday = rtc_tm.tm_mday;
    tp->tm_mon = rtc_tm.tm_mon;
    tp->tm_year = rtc_tm.tm_year;
    tp->tm_wday = rtc_tm.tm_wday;
    tp->tm_yday = rtc_tm.tm_yday;
    tp->tm_isdst = rtc_tm.tm_isdst;

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : port_time()
// Description    : NULL 입력 시 현재 시간의 UTC time_t 를 반환
//------------------------------------------------------------------------------
port_time_t port_time(port_tm_t *tp)
{
    port_time_t time = 0;
    port_tm_t tm;

    if (tp == NULL) {
        port_get_rtc_time(&tm);
    } else {
        memcpy(&tm, tp, sizeof(port_tm_t));
    }

    time = port_mktime(&tm);

    return time;
}

//------------------------------------------------------------------------------
// Function Name  : port_exist_file()
// Description    :
//------------------------------------------------------------------------------
int port_exist_file(const char * _path)
{
    struct stat sb;

    memset(&sb, 0, sizeof(struct stat));
    
    stat(_path, &sb);
    if (S_ISREG(sb.st_mode))
        return 1;

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : port_fw_path()
// Description    :
//------------------------------------------------------------------------------
int port_fw_path(char *_prefix, char *_path)
{
    int res = 0;    
    DIR *dp = NULL;
    struct dirent *d_entry = NULL;
    char file_n[] = "Adaptor_";

    port_printf_co(CO_BLUE, "<%s> prefix: %s\r\n", __func__, _prefix);
    
    mkdir(_prefix, 0755);
    dp = opendir(_prefix);
    if (dp == NULL) {
        port_printf_co(CO_BLUE, "<%s> invalid path !!\r\n", __func__);
        return 0;
    }
    port_printf_co(CO_BLUE, "<%s> at %d\r\n", __func__, __LINE__);
    while (NULL != (d_entry = readdir(dp))) {
        if (strncmp(d_entry->d_name, file_n, strlen(file_n)) == 0) {
            strcpy(_path, _prefix);
            port_path_strcat(_path, d_entry->d_name);
            res = strlen(_path)+1;
            break;
        }
    }

    if (res > 0)
        port_printf_co(CO_BLUE, "<%s> %s, %d", __func__, _path, res);

    closedir(dp);
    return res;
}

//------------------------------------------------------------------------------
// Function Name  : port_rcv_msg()
// Description    :
//------------------------------------------------------------------------------
int port_rcv_msg(MSG_KEY msg_key, port_mq_t *_msg)
{
    #if defined(PLAT_ANDROID_LINUX)
    ITC_MSG_LIST *msg_list_ptr;

    if ((msg_list_ptr = msg_find_id(msg_key)) != NULL) {
        _msg->type = msg_list_ptr->msg_ptr->type;
        _msg->msg_id = msg_list_ptr->msg_ptr->msg_id;
        _msg->param[0] = msg_list_ptr->msg_ptr->param[0];
        _msg->param[1] = msg_list_ptr->msg_ptr->param[1];
        msg_remove_queue(msg_list_ptr);
        return 1;
    }

    return 0;
    #else
    return msgrcv(msg_key, (void *)_msg,
        IPC_MSG_SIZE, IPC_MSG_TYPE, IPC_NOWAIT);
    #endif
}

//------------------------------------------------------------------------------
// Function Name  : port_send_msg()
// Description    : msg send to obd thread
//------------------------------------------------------------------------------
int port_send_msg(MSG_KEY msg_key,        uint32_t msg, uint32_t param1, uint32_t param2)
{
    port_mq_t t_msg;

    memset(&t_msg, 0, sizeof(port_mq_t));
    
    t_msg.type = IPC_MSG_TYPE;
    t_msg.msg_id = msg;
    t_msg.param[0] = param1;
    t_msg.param[1] = param2;

    #if defined(PLAT_ANDROID_LINUX)
    if (msg_append_queue(msg_key, &t_msg) != NULL)
        return 0;

    return -1;
    #else
    return msgsnd(msg_key, (void *)&t_msg,
            IPC_MSG_SIZE, IPC_NOWAIT);   
    #endif
}

//------------------------------------------------------------------------------
// Function Name  : port_create_msg()
// Description    : create message queue
//------------------------------------------------------------------------------
int port_create_msg(MSG_KEY *msg_key)
{
    port_mq_t msg;
    
    #if defined(PLAT_ANDROID_LINUX)
    *msg_key = msg_create_queue();
    #else
    *msg_key = msgget(s_mq_key++, IPC_CREAT|0666);
    // discard garbage message..
    while (port_rcv_msg(*msg_key, &msg) > 0) {
    }
    #endif
    
    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : port_destroy_msg()
// Description    : destroy message queue
//------------------------------------------------------------------------------
int port_destroy_msg(MSG_KEY *msg_key)
{
    #if defined(PLAT_ANDROID_LINUX)
    g_msg_init_done = 0;
    pthread_mutex_destroy(&msg_access_sync);
    return msg_destroy_all_queues();
    #else
    return 0;
    #endif
}

//------------------------------------------------------------------------------
// Function Name  : port_create_mutex()
// Description    : create mutex
//------------------------------------------------------------------------------
int port_create_mutex(MUTEX_P *mutex)
{
    *mutex = port_malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init((pthread_mutex_t *)*mutex, NULL);

    return 1;
}

//------------------------------------------------------------------------------
// Function Name  : port_destroy_mutex()
// Description    : destroy mutex
//------------------------------------------------------------------------------
int port_destroy_mutex(MUTEX_P *mutex)
{
    port_free(*mutex);

    return 1;
}

//------------------------------------------------------------------------------
// Function Name  : port_create_thread()
// Description    : create thread
//------------------------------------------------------------------------------
int port_create_thread(port_thread_info_t *_tsk_info, void *param)
{
    int policy = 0, ret, max_prio;
    struct sched_param sched_param;

    _tsk_info->state = 1;
    pthread_create((pthread_t *)&_tsk_info->tsk_id,
        NULL, (void*)_tsk_info->handler, param);

    max_prio = sched_get_priority_max(SCHED_FIFO);
    if (_tsk_info->priority <= max_prio) {
        sched_param.sched_priority = _tsk_info->priority;    
        // Attempt to set thread real-time priority to the SCHED_FIFO policy
        ret = pthread_setschedparam(_tsk_info->tsk_id, SCHED_FIFO, &sched_param);
        if (ret != 0) {
            // Print the error
            port_printf("Unsuccessful in setting thread realtime prio");
            return 0;     
        }         

        // Now verify the change in thread priority
        ret = pthread_getschedparam(_tsk_info->tsk_id, &policy, &sched_param);
        if (ret != 0) {
            port_printf("Couldn't retrieve real-time scheduling parameters");
            return 0;
        }

        // Check the correct policy was applied
        if(policy != SCHED_FIFO) {
            port_printf("Scheduling is NOT SCHED_FIFO!");
        } else {
           port_printf("SCHED_FIFO OK");
        }

        port_printf("Priority: %d", sched_param.sched_priority);
    }
    
    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : port_destroy_thread()
// Description    : destroy thread
//------------------------------------------------------------------------------
int port_destroy_thread(port_thread_info_t *_tsk_info)
{
    int status;
    
    _tsk_info->state = 0;
    pthread_join(_tsk_info->tsk_id, (void **)&status);

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : port_resume_thread()
// Description    : resume thread
//------------------------------------------------------------------------------
int port_resume_thread(port_thread_info_t *_tsk_info)
{
    // TODO: TBD

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : port_suspend_thread()
// Description    : suspend thread
//------------------------------------------------------------------------------
int port_suspend_thread(port_thread_info_t *_tsk_info)
{
    // TODO: TBD

    return 0;
}

/* 
    리눅스 타이머 시그널을 사용할 경우 다른 시그날 후킹과 겹쳐 이상동작을 한다. 사용하지 말 것 !!
*/
int port_create_timer(port_timer_info_t *_tmr_info)
{
    struct sigaction sigac;
    struct sigevent sigev;

    memset(&sigac, 0, sizeof(sigac));
    sigemptyset(&sigac.sa_mask);

    memset(&sigev, 0, sizeof(sigev));

    sigac.sa_sigaction = (void *)(_tmr_info->handler);
    sigac.sa_flags = SA_SIGINFO;

    if (sigaction(SIGRTMIN, &sigac, NULL) == -1) {
        port_printf_co(CO_RED, "<%s> tmr_id: 0x%X, sigaction() failed !!!", __FUNCTION__, _tmr_info->tmr_id);
        return -1;
    }

    sigev.sigev_notify = SIGEV_SIGNAL;
    sigev.sigev_signo = SIGRTMIN;
    if (timer_create(CLOCK_REALTIME, &sigev, (timer_t)&(_tmr_info->tmr_id)) == -1) {
        port_printf_co(CO_RED, "<%s> tmr_id: 0x%X, timer_create() failed !!!", __FUNCTION__, _tmr_info->tmr_id);
        return 0;
    }

    port_printf_co(CO_BLUE, "<%s>", __FUNCTION__);
    port_printf_co(CO_BLUE, ">> tmr_id: 0x%X", _tmr_info->tmr_id);
    port_printf_co(CO_BLUE, ">> args  : %d", _tmr_info->args);
    port_printf_co(CO_BLUE, ">> ticks : %d ms", _tmr_info->ticks);
    port_printf_co(CO_BLUE, ">> (%d byte) sizeof(_tmr_info->tmr_id)", sizeof(_tmr_info->tmr_id));
    port_printf_co(CO_BLUE, ">> (%d byte) sizeof(timer_t)", sizeof(timer_t));

    return 1;
}

int port_delete_timer(port_timer_info_t *_tmr_info)
{
    if (timer_delete((timer_t)(_tmr_info->tmr_id)) == -1) {
        port_printf_co(CO_RED, "<%s> tmr_id: 0x%X, timer_delete() failed !!!", __FUNCTION__, _tmr_info->tmr_id);
        return 0;
    }

    return 1;
}

int port_stop_timer(port_timer_info_t *_tmr_info)
{
    struct itimerspec rt_itspec;

    rt_itspec.it_value.tv_sec = 0;
    rt_itspec.it_value.tv_nsec = 0;
    rt_itspec.it_interval.tv_sec = 0;
    rt_itspec.it_interval.tv_nsec = 0;

    if (timer_settime((timer_t)(_tmr_info->tmr_id), 0, &rt_itspec, NULL) == -1) {
        port_printf_co(CO_RED, "<%s> tmr_id: 0x%X, timer_settime() failed !!!", __FUNCTION__, _tmr_info->tmr_id);
        return 0;
    }

    return 1;
}

int port_start_timer(port_timer_info_t *_tmr_info)
{
    struct itimerspec rt_itspec;

    rt_itspec.it_value.tv_sec = _tmr_info->ticks / 1000;
    rt_itspec.it_value.tv_nsec = (_tmr_info->ticks%1000)*1000*1000;
    rt_itspec.it_interval.tv_sec = _tmr_info->ticks / 1000;
    rt_itspec.it_interval.tv_nsec = (_tmr_info->ticks%1000)*1000*1000;
    
    if (timer_settime((timer_t)(_tmr_info->tmr_id), 0, &rt_itspec, NULL) == -1) {
        port_printf_co(CO_RED, "<%s> tmr_id: 0x%X, timer_settime() failed !!!", __FUNCTION__, _tmr_info->tmr_id);
        return 0;
    }

    return 1;
}

int port_create_event(uint32_t *eid)
{
    struct evflag *events;
    int ercd;

    events = (struct evflag *)port_malloc(sizeof(struct evflag));
    *eid =(uint32_t)(long)events;

    pthread_mutex_init(&events->mtx, NULL);
    pthread_cond_init(&events->var, NULL);

    //port_printf_co(CO_BLUE, "<%s>", __FUNCTION__);
    //port_printf_co(CO_BLUE, ">> eid: 0x%X", *eid);
    //port_printf_co(CO_BLUE, ">> (%d byte) sizeof(uint32_t)", sizeof(uint32_t));

    return 1;
}

int port_delete_event(uint32_t eid)
{
    struct evflag *events = (struct evflag *)(long)eid;

    port_free((void *)events);

    return 1;
}

/*
 * port_set_event 을 동시에 사용하는 처리가 많아질 경우
 * count semaphore 구현이 필요할 수 있다.
 *
 */
int port_wait_event(uint32_t eid, uint32_t type, uint32_t option, uint32_t *flags, uint32_t timeout)
{
    struct evflag *events = (struct evflag *)(long)eid;
    struct timeval now;
    struct timespec ts;
    int ercd = 0;
    uint32_t already = 0;

    pthread_mutex_lock(&events->mtx);
    already = (events->flags & type);
    if (already == 0) {
        if (PORT_EVT_WAIT_FOREVER != timeout) {
            clock_gettime(CLOCK_REALTIME, &ts);
            ts.tv_sec += timeout;
            ercd = pthread_cond_timedwait(&events->var, &events->mtx, &ts);
        } else {
            ercd = pthread_cond_wait(&events->var, &events->mtx);
        }

        if (ETIMEDOUT == ercd) {
            port_printf_co(CO_RED, "<%s> timedout !", __func__);
        }
    }
    events->flags &= (~type);
    pthread_mutex_unlock(&events->mtx);

    return ercd;
}

int port_set_event(uint32_t eid, uint32_t type)
{
    struct evflag *events = (struct evflag *)(long)eid;
    int ercd;

    pthread_mutex_lock(&events->mtx);
    events->flags |= type;
    ercd = pthread_cond_signal(&events->var);
    pthread_mutex_unlock(&events->mtx);

    return 1;
}

int port_clr_event(uint32_t eid, uint32_t type)
{
    struct evflag *events = (struct evflag *)(long)eid;
    int ercd;

    pthread_mutex_lock(&events->mtx);
    events->flags &= ~type;
    pthread_mutex_unlock(&events->mtx);

    return 1;
}

int port_check_event(uint32_t eid, uint32_t type)
{
    struct evflag *events = (struct evflag *)(long)eid;
    int ercd;

    pthread_mutex_lock(&events->mtx);
    ercd = events->flags & type;
    pthread_mutex_unlock(&events->mtx);

    return ercd;
}

#if 1
//------------------------------------------------------------------------------
// Function Name  : port_printf_co()
// Description    : color printf
//------------------------------------------------------------------------------
void port_printf_co(int color, const char *fmt, ...)
{
#if defined(PLAT_ANDROID_LINUX)
    char _buf[1024] = {0,};
    va_list args;

    va_start(args, fmt);
    vsnprintf(_buf, 1023, fmt, args);
    va_end(args);
    __android_log_print(ANDROID_LOG_ERROR, "twdev", "%s", _buf);
#else
    char _buf[1024] = {0,};
    va_list args;
    int len;
    unsigned long t = port_mono_time();

    switch (color) {
    case CO_RED:
    case CO_GREEN:
    case CO_BROWN:
    case CO_BLUE:
    case CO_PURPLE:     
    case CO_CYAN:
        sprintf(_buf, "%s[%08u] ", col_str[color], t);
        break;
    default:
        sprintf(_buf, "%s[%08u] ", col_str[CO_DEFAULT], t);
        break;
    }

    len = strlen(_buf);
    va_start(args, fmt);
    vsnprintf(_buf + len, 1024 - len - 1, fmt, args);
    va_end(args);
    fprintf(stdout, "%s%s\r\n", _buf, col_str[CO_DEFAULT]);
#endif
}

//------------------------------------------------------------------------------
// Function Name  : port_printf()
// Description    : printf
//------------------------------------------------------------------------------
void port_printf(const char *fmt, ...)
{
    char _buf[1024] = {0,};
    va_list args;

    va_start(args, fmt);
    vsnprintf(_buf, 1023, fmt, args);
    va_end(args);
#if defined(PLAT_ANDROID_LINUX)
    __android_log_print(ANDROID_LOG_INFO, "twdev", "%s", _buf);
#else
    port_printf_co(CO_DEFAULT, _buf);  
#endif
}
#endif

//------------------------------------------------------------------------------
// Function Name  : port_printf_nnt()
// Description    : printf not newline & timestamp
//------------------------------------------------------------------------------
void port_printf_nnt(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}

//------------------------------------------------------------------------------
// Function Name  : port_malloc()
// Description    : 
//------------------------------------------------------------------------------
void *port_malloc(int size)
{
    void *mem;

    mem = malloc(size);
    
    return mem;
}

//------------------------------------------------------------------------------
// Function Name  : port_free()
// Description    : 
//------------------------------------------------------------------------------
void port_free(void *mem)
{
    free(mem);
}

//------------------------------------------------------------------------------
// Function Name  : port_mem_pool_info()
// Description    : 
//------------------------------------------------------------------------------
void port_mem_pool_info(void)
{
}

//------------------------------------------------------------------------------
// Function Name  : port_mutex_lock()
// Description    : 
//------------------------------------------------------------------------------
int port_mutex_lock(MUTEX_P mutex)
{
    return pthread_mutex_lock((pthread_mutex_t *)mutex);
}

//------------------------------------------------------------------------------
// Function Name  : port_mutex_unlock()
// Description    : 
//------------------------------------------------------------------------------
int port_mutex_unlock(MUTEX_P mutex)
{
    return pthread_mutex_unlock((pthread_mutex_t *)mutex);
}

//------------------------------------------------------------------------------
// Function Name  : port_msleep()
// Description    : 
//------------------------------------------------------------------------------
void port_msleep(uint32_t ms)
{
    usleep(ms*1000);
}

//------------------------------------------------------------------------------
// Function Name  : port_fopen()
// Description    : 
//------------------------------------------------------------------------------
FILE *port_fopen(const char *name, const char *mode)
{
    return fopen(name, mode);
}

//------------------------------------------------------------------------------
// Function Name  : port_fwrite()
// Description    : 
//------------------------------------------------------------------------------
int port_fwrite(const void *ptr, uint32_t size, uint32_t nobj, FILE *stream)
{
    return fwrite(ptr, size, nobj, (FILE *)stream);
}

//------------------------------------------------------------------------------
// Function Name  : port_fread()
// Description    : 
//------------------------------------------------------------------------------
int port_fread(void *ptr, uint32_t size, uint32_t nobj, FILE *stream)
{
    return fread(ptr, size, nobj, (FILE *)stream);
}

//------------------------------------------------------------------------------
// Function Name  : port_fgets()
// Description    : 
//------------------------------------------------------------------------------
char *port_fgets(char *ptr, uint32_t bsize, FILE *stream)
{
    return fgets(ptr, bsize, (FILE *)stream);
}

//------------------------------------------------------------------------------
// Function Name  : port_fseek()
// Description    : 
//------------------------------------------------------------------------------
int port_fseek(FILE *stream, long long offset, int origin)
{
    return fseek((FILE *)stream, offset, origin);
}

//------------------------------------------------------------------------------
// Function Name  : port_ftell()
// Description    : 
//------------------------------------------------------------------------------
uint32_t port_ftell(FILE *stream)
{
    return ftell((FILE *)stream);
}

//------------------------------------------------------------------------------
// Function Name  : port_fsync()
// Description    : 
//------------------------------------------------------------------------------
int port_fsync(FILE *stream)
{
    return -1;//fsync((int)stream);
}

//------------------------------------------------------------------------------
// Function Name  : port_access()
// Description    : 
//------------------------------------------------------------------------------
int port_access(const char *_path, int mode)
{
    return access(_path, mode);
}

//------------------------------------------------------------------------------
// Function Name  : port_fclose()
// Description    : 
//------------------------------------------------------------------------------
int port_fclose(FILE *stream)
{
    return fclose((FILE *)stream);
}

#define CP_BUF_SIZE             (128 * 1024)    // 128KB
//------------------------------------------------------------------------------
// Function Name  : port_copy()
// Description    : 
//------------------------------------------------------------------------------
uint32_t port_copy(const char * src_file, const char * dst_file)
{
    int rval, ret;
    char *buf = NULL;
    FILE *fin = NULL;
    FILE *fout = NULL;

    fin = port_fopen(src_file, "r");
    if (fin == NULL) {
        port_printf("src file open error %s", src_file);
        ret = -1;
        goto _COPY_END;
    }

    fout = port_fopen(dst_file, "w");
    if (fout == NULL) {
        port_printf("dest file open error %s", dst_file);
        ret = -1;
        goto _COPY_END;
    }

    buf = port_malloc(CP_BUF_SIZE);
    if (buf == NULL) {
        port_printf("cannot alloc buf(size:%d(0x%x)", CP_BUF_SIZE, CP_BUF_SIZE);
        ret = -1;
        goto _COPY_END;
    }

    do {
        //memset(buf, 0x00, CP_BUF_SIZE);
        rval = port_fread(buf, sizeof(char), CP_BUF_SIZE, fin);
        if (rval > 0) {
            rval = port_fwrite(buf, sizeof(char), rval, fout);
            if (rval <= 0) {
                port_printf("file write error");
                ret = -1;
                goto _COPY_END;
            }
        }
    } while (rval > 0);
    ret = 0;

_COPY_END:
    if (fin) 
        port_fclose(fin);
    if (fout)
        port_fclose(fout);
    if (buf)
        port_free(buf);

    return ret;
}

//------------------------------------------------------------------------------
// Function Name  : port_mkdir()
// Description    : 
//------------------------------------------------------------------------------
int port_mkdir(const char *name)
{
    return mkdir(name, 0755);
}

//------------------------------------------------------------------------------
// Function Name  : port_remove()
// Description    : 
//------------------------------------------------------------------------------
int port_remove(const char *name)
{
    return remove(name);
}

//------------------------------------------------------------------------------
// Function Name  : port_path_strcat()
// Description    :
//------------------------------------------------------------------------------
void port_path_strcat(char *_dst, char *_append)
{
    strcat(_dst, "/");
    strcat(_dst, _append);
}

//------------------------------------------------------------------------------
// Function Name  : port_copy_romfs()
// Description    :
//------------------------------------------------------------------------------
int port_copy_romfs(const char *_src_path, const char *_dst_path)
{
    return port_copy(_src_path, _dst_path);
}

//------------------------------------------------------------------------------
// Function Name  : port_fread_romfs()
// Description    :
//------------------------------------------------------------------------------
int port_fread_romfs(void *ptr, uint32_t size, uint32_t nobj, const char *_path)
{
    FILE *fd = NULL;
    int rval;
    
    fd = port_fopen(_path, "r");
    if (fd == NULL) {
        port_printf_co(CO_RED,"<%s> %s file open failed", __func__, _path);
        return -1;
    }

    rval = port_fread(ptr, size, nobj, fd);
    port_fclose(fd);
    
    return rval;
}

//------------------------------------------------------------------------------
// Function Name  : port_rand_in_range(int min, int max)
// Description    :
//------------------------------------------------------------------------------
int port_rand_in_range(int min, int max)
{
    unsigned int seed = 0;
    int rand_val = 0;

    seed = port_time(NULL);
    srand(seed);
    rand_val = rand() % ((max  - min) + 1) + min;

    return rand_val;
}

//------------------------------------------------------------------------------
// Function Name  : port_copy_file()
// Description    :
//------------------------------------------------------------------------------
int port_copy_file(const char *src, const char *dst) 
{
    int source = open(src, O_RDONLY, 0);
    int dest = open(dst, O_WRONLY | O_CREAT, 0644);
    int res;
    
    // struct required, rationale: function stat() exists also
    struct stat stat_source;
    fstat(source, &stat_source);

    res = sendfile(dest, source, 0, stat_source.st_size);

    close(source);
    close(dest);

    return res;
}

int port_file_size(const char *path)
{
    int retFileSize = 0;
    FILE *fp = NULL;

    fp = port_fopen(path, "rb");
    if (fp == NULL) {
        return 0;
    }
    port_fseek(fp, 0, SEEK_END);
    retFileSize = port_ftell(fp);
    port_fseek(fp, 0, SEEK_SET);
    port_fclose(fp);

    return retFileSize;
}


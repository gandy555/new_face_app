/******************************************************************************
 * Filename:
 *   controller_serial.c
 *
 * Description:
 *   controller of uart driver 
 *
 * Author:
 *   gandy
 *
 * Version : V0.1_15-10-02
 * ---------------------------------------------------------------------------
 * Abbreviation
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#include "type.h"
#include "port.h"
#include "serial.h"

/******************************************************************************
 *
 * Functions Declaration
 *
 ******************************************************************************/
int serial_open(const char *_dev, u32 _baud);
void serial_close(int _fd);
int serial_write(int _fd, const u8 *_data, u32 _size);
int serial_read(int _fd, u8 *_data, u32 _size);

//------------------------------------------------------------------------------
// Function Name  : serial_open()
// Description    :
//------------------------------------------------------------------------------
int serial_open(const char *_dev, u32 _baud)
{
    struct termios tio;
    int fd;
    
    if (_dev == NULL) {
        port_printf("<%s> invalid dev name", __func__);
        return -1;
    }

    fd = open(_dev, O_RDWR);    //|O_NONBLOCK);
    if (fd < 0) {
        port_printf("<%s> '%s' device open failure", __func__, _dev);
        return fd;
    }

    memset(&tio, 0, sizeof(struct termios));
    tio.c_iflag = IGNPAR;               // non-parity
    tio.c_oflag = 0;                    //
    tio.c_cflag = CS8 | CLOCAL | CREAD; // NO-rts/cts   
    tio.c_cflag |= _baud;
    tio.c_lflag = 0;

    tio.c_cc[VTIME] = 1;
    tio.c_cc[VMIN]  = 1; 

    tcsetattr(fd, TCSANOW, &tio);
    tcflush(fd, TCIFLUSH);
    
    port_printf("<%s> '%s' device open success", __func__, _dev);

    return fd;
}

//------------------------------------------------------------------------------
// Function Name  : serial_close()
// Description    :
//------------------------------------------------------------------------------
void serial_close(int _fd)
{
    if (_fd >= 0) 
        close(_fd);
}

//------------------------------------------------------------------------------
// Function Name  : serial_write()
// Description    :
//------------------------------------------------------------------------------
int serial_write(int _fd, const u8 *_data, u32 _size)
{
    int ret = ERROR;
    
    ret = write(_fd, _data, _size);
    if (ret == ERROR) {
        port_printf("<%s> write failure : errno=%d %s",
            __func__, errno, strerror(errno));
        return ERROR;
    }
    
    tcflush(_fd, TCIFLUSH); 
    
    return ret;
}

//------------------------------------------------------------------------------
// Function Name  : serial_read()
// Description    :
//------------------------------------------------------------------------------
int serial_read(int _fd, u8 *_data, u32 _size)
{
    int ret = ERROR;

    ret = read(_fd, _data, _size);
    if (ret == ERROR) {
        /*port_printf("<%s> read failure : errno=%d %s",
            __func__, errno, strerror(errno));*/
        return ERROR;
    }

    return ret;
}


/******************************************************************************
 * Filename:
 *   controller_rtc.c
 *
 * Description:
 *   control the rtc
 *
 * Author:
 *   gandy
 *
 * Version : V0.1_16-03-12
 * ---------------------------------------------------------------------------
 * Abbreviation
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>
#include <signal.h>
#include <time.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/rtc.h>

#include "type.h"
#include "port.h"
#include "rtc.h"
/******************************************************************************
 *
 * Variable Declaration
 *
 ******************************************************************************/
#define RTC_DEV             "/dev/rtc0"

#define RTC_TICK_ON      _IO('p', 0x03)  /* Update int. enable on        */
#define RTC_TICK_OFF     _IO('p', 0x04)  /* ... off                      */
#define RTC_TICK_SET      _IO('p', 0x05)  /* Periodic int. enable on      */
#define RTC_TICK_READ     _IO('p', 0x06)  /* ... off                      */

static int g_rtc_fd = -1;

/******************************************************************************
 *
 * Public Functions Declaration
 *
 ******************************************************************************/
int rtc_init(void);
int systime_to_rtc(void);

//------------------------------------------------------------------------------
// Function Name  : rtc_init()
// Description    :
//------------------------------------------------------------------------------
int rtc_init(void)
{
    g_rtc_fd = open(RTC_DEV, O_RDONLY);
    if (g_rtc_fd < 0)   {
        port_printf_co(CO_RED, "<%s> open failed(%s)", __func__, strerror(errno));
        return -1;
    }

    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : systime_to_rtc()
// Description    :
//------------------------------------------------------------------------------
int systime_to_rtc(void)
{
    struct tm sys_tm = {0,};
    struct rtc_time rtc_tm;
    time_t now;
    int retval;
    
    if (g_rtc_fd < 0) {
        port_printf_co(CO_RED, "<%s> file handle error", __func__);
        return -1;
    }

    now = time(NULL);
    localtime_r(&now, &sys_tm);
    rtc_tm.tm_year = sys_tm.tm_year;
    rtc_tm.tm_mon = sys_tm.tm_mon;
    rtc_tm.tm_mday = sys_tm.tm_mday;
    rtc_tm.tm_hour = sys_tm.tm_hour;
    rtc_tm.tm_min = sys_tm.tm_min;
    rtc_tm.tm_sec = sys_tm.tm_sec;

    retval = ioctl(g_rtc_fd, RTC_SET_TIME, &rtc_tm);
    if (retval < 0) {
        port_printf_co(CO_RED, "ioctl RTC_SET_TIME  faild!!!");
        return -2;
    }

    return 0;
}


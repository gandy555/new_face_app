/******************************************************************************
 * Filename:
 *   app_obd_svc.c
 *
 * Description:
 *   obd service application
 *
 * Author:
 *   gandy
 *
 * Version : V0.1_15-02-03
 * ---------------------------------------------------------------------------
 * Abbreviation
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>
#include <linux/input.h>
#include <pthread.h>
#include <sys/mman.h>
#include <poll.h>

#include "type.h"
#include "port.h"
#include "gpio.h"
/******************************************************************************
 *
 * Variable Declaration
 *
 ******************************************************************************/
#define GPIO_SYSFS_PATH "/sys/class/gpio"
#define GPIO_SYSFS_EXPORT   GPIO_SYSFS_PATH"/export"

#define GPIO_HIGH   1
#define GPIO_LOW   0
/******************************************************************************
 *
 * Public Functions Declaration
 *
 ******************************************************************************/
void gpio_config(u32 _port, u32 _pin, u32 _func);
u8 gpio_get(u32 _port, u32 _pin);
void gpio_set(u32 _port, u32 _pin);
void gpio_clr(u32 _port, u32 _pin);

static int export_gpio_pin(u32 _num)
{
    int fd;
    int len;
    char buf[10] = {0,};
    
    if ((fd = open(GPIO_SYSFS_EXPORT, O_WRONLY)) < 0) {
        port_printf_co(CO_RED, "<%s> cannot export gpio%d", __func__, _num);
        return -1;
    }

    len = snprintf(buf, sizeof(buf), "%d", _num);
    if (write(fd, buf, len) < 0) { 
        port_printf_co(CO_RED, "<%s> cannot write gpio%d export file", __func__, _num);
        close(fd);
        return -1;
    }
    
    close(fd);
    return 0;
}

static int set_gpio_dir(u32 _num, u32 _dir)
{
    int fd;
    int len;
    char buf[64] = {0,};

    len = snprintf(buf, sizeof(buf), GPIO_SYSFS_PATH"/gpio%d/direction", _num);
    
    fd = open(buf, O_WRONLY);
    if (fd < 0) {
        port_printf_co(CO_RED, "<%s> Cannot open direction file.", __func__);
        return -1;
    }
        
    len = snprintf(buf, sizeof(buf), "%s", (_dir == GPIO_IN) ? "in" : "out");
    if (write(fd, buf, len) < 0) {
        port_printf_co(CO_RED, "<%s> Cannot write direction.", __func__);
        close(fd);
        return -1;
    }
    
    close(fd);
    return 0;
}

static int set_gpio_out(u32 _num, u32 value)
{
    int fd;
    int len;
    char buf[64] = {0,};

    len = snprintf(buf, sizeof(buf), GPIO_SYSFS_PATH"/gpio%d/direction", _num);
    
    fd = open(buf, O_WRONLY);
    if (fd < 0) {
        port_printf_co(CO_RED, "<%s> Cannot open direction file.", __func__);
        return -1;
    }

    memset(buf, sizeof(buf), 0);
    len = snprintf(buf, sizeof(buf), "%s", (value == 1) ? "high" : "low");
    if (write(fd, buf, len) < 0) {
        port_printf_co(CO_RED, "<%s> Cannot write direction.", __func__);
        close(fd);
        return -1;
    }
    
    close(fd);
    return 0;
}

static int get_gpio_val(u32 _num)
{
    int fd;
    int len;
    char buf[64] = {0,};
    
    len = snprintf(buf, sizeof(buf), GPIO_SYSFS_PATH"/gpio%d/value", _num);

    fd = open(buf, O_RDWR);
    if (fd < 0) {
        port_printf_co(CO_RED, "<%s> Cannot open value file.", __func__);
        return -1;
    }

    len = read(fd, buf, sizeof(buf));
    if (len < 0) {
        port_printf_co(CO_RED, "<%s> Cannot read value file.", __func__);
        close(fd);
        return -1;
    }
    
    close(fd);
    return atoi(buf);
}

static int set_gpio_val(u32 _num, int _val)
{
    int fd;
    int len;
    char buf[64] = {0,};

    len = snprintf(buf, sizeof(buf), GPIO_SYSFS_PATH"/gpio%d/value", _num); 
    fd = open(buf, O_RDWR);
    if (fd < 0) {
        port_printf_co(CO_RED, "<%s> Cannot open value file.", __func__);
        return -1;
    }

    len = snprintf(buf, sizeof(buf), "%d", _val);
    if (write(fd, buf, len) < 0) {
        port_printf_co(CO_RED, "<%s> Cannot write value file.", __func__);
        close(fd);
        return -1;
    }
    
    close(fd);
    return 0;
}

//------------------------------------------------------------------------------
// Function Name  : gpio_get()
// Description    :
//------------------------------------------------------------------------------
void gpio_config(u32 _port, u32 _pin, u32 _dir)
{
    u32 num;

    num = _port + _pin;
    export_gpio_pin(num);
    set_gpio_dir(num, _dir);
}

//------------------------------------------------------------------------------
// Function Name  : gpio_config_out_value()
// Description    :
//------------------------------------------------------------------------------
void gpio_config_out_value(u32 _port, u32 _pin, u32 val)
{
    u32 num;

    num = _port + _pin;
    export_gpio_pin(num);
    set_gpio_out(num, val);
}

//------------------------------------------------------------------------------
// Function Name  : gpio_get()
// Description    :
//------------------------------------------------------------------------------
u8 gpio_get(u32 _port, u32 _pin)
{
    u32 num;

    num = _port + _pin;
        
    if (get_gpio_val(num))
        return HIGH;

    return LOW;
}

//------------------------------------------------------------------------------
// Function Name  : gpio_set()
// Description    :
//------------------------------------------------------------------------------
void gpio_set(u32 _port, u32 _pin)
{
    u32 num;

    num = _port + _pin;
    set_gpio_val(num, 1);
}

//------------------------------------------------------------------------------
// Function Name  : gpio_get()
// Description    :
//------------------------------------------------------------------------------
void gpio_clr(u32 _port, u32 _pin)
{
    u32 num;

    num = _port + _pin;
    set_gpio_val(num, 0);
}

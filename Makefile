TARGET_PLATFORM_ARM_LINUX = y

ifeq ($(TARGET_PLATFORM_ARM_LINUX),y)
SYSROOT = ../build-cl-som-imx8-fsl-imx-wayland/tmp/work/aarch64-poky-linux/make/4.2.1-r0/recipe-sysroot
PREFIX = ${SYSROOT}-native/usr/bin/aarch64-poky-linux/aarch64-poky-linux-
endif
CC = ${PREFIX}gcc
CXX = ${PREFIX}g++
STRIP = ${PREFIX}strip
NM = ${PREFIX}nm

SRC_TOP_DIR = ./src
LIBSDIR = -L./libs
BUILDDIR = ./obj
MKDIR = mkdir -p $(BUILDDIR)

HEADERDIR = -I./include -I$(SRC_TOP_DIR)/utils/include
TARGET = app_newface
TARGET_MAP = $(TARGET).map

LIBS = -lrt -lpthread -lm
ifeq ($(TARGET_PLATFORM_ARM_LINUX),y)
LIBS += -lpng16 -lz
DEFINES += -DCONFIG_USE_LIB_PNG
endif
CFLAGS = --sysroot=${SYSROOT} -Wno-unused-function -Wno-format -Wno-unused-result
CFLAGS += -O1 -g $(DEFINES)

export CFLAGS SRCS SRC_TOP_DIR
SRCS = $(SRC_TOP_DIR)/main.c\
		$(SRC_TOP_DIR)/msg.c\
		$(SRC_TOP_DIR)/port.c\
		$(SRC_TOP_DIR)/workqueue.c\
		$(SRC_TOP_DIR)/gpio.c\
		$(SRC_TOP_DIR)/rtc.c\
		$(SRC_TOP_DIR)/serial.c\
		$(SRC_TOP_DIR)/watchdog.c\
		$(SRC_TOP_DIR)/user_if.c\
		$(SRC_TOP_DIR)/tcp_api.c\
		$(SRC_TOP_DIR)/udp_api.c\
		$(SRC_TOP_DIR)/ftp_api.c\
		$(SRC_TOP_DIR)/spi_dev.c\
		$(SRC_TOP_DIR)/utils.c
		
OBJS := $(SRCS:.c=.o)
			
$(TARGET):$(OBJS)
	$(CC) -o $(TARGET) $(addprefix $(BUILDDIR)/, $(notdir $^)) $(CFLAGS) $(LIBSDIR) $(LIBS)
	$(NM) -n $(TARGET) > $(TARGET_MAP)
	
%.o:%.c
	$(MKDIR)
	$(CC) $(CFLAGS) $(HEADERDIR) -c $< -o $(addprefix $(BUILDDIR)/, $(notdir $@)) 

echo:
	@echo $(OBJS)

clean :
	rm -rf $(BUILDDIR)/*.o $(TARGET)
